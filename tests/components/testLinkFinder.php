<!DOCTYPE html>
<html>
  <head>
    <title>Page de test du "LinkFinder" !</title>
    <?php require "../../vendor/autoload.php";
    
    use Ultimitaries\Debug\Tester,
        Ultimitaries\Components\LinkFinder;
    
    $debug = new Tester;
    ?>
  </head>
  <body>
    <?php // Décommentez les instructions pour pouvoir tester chaque méthode indépendamment des autres !
    $fileToFound = "SQL.php";
    new LinkFinder("/");
    new LinkFinder("ultimitaries");
    new LinkFinder("html");
    $test = new LinkFinder("Ultimitaries",false); // Laisser à faux pour obtenir les résultats les plus "lents"
    echo $test->getLinkFor($fileToFound).BR2;
    echo $test->getLinkFor("BP_Query.php").BR2; // null
    echo $test->getLinkFor($fileToFound,"Storages").BR2;
    echo $test->getLinkFor($fileToFound,"core","Storages").BR2;
    echo $test->getLinkFor($fileToFound,"Storages","Core").BR2; // null
    echo $test->getLinkFor($fileToFound,"debug","core").BR2; // null
    echo $test->getLinkFor("debug").BR2;
    echo $test->getLinkFor("composer.json").BR2;
    echo $test->getLinkFor("testParser.json").BR2;
    echo $test->getLinkFor("testLinkFinder.php").BR2;
    echo $test->getLinkFor("random").BR2;
    echo $test->getLinkFor("fichierSansExtension").BR2;
    echo $test->getLinkFor("test.txt","components").BR2;
    var_dump($test);
    ?>
  </body>
</html>
