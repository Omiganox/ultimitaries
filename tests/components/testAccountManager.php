<?php session_start(); ?>
<!DOCTYPE html>
<html>
  <head>
    <title>Page de test de l'AccountManager !</title>
    <?php $doubleSaut = "<br/><br/>";
    foreach(array("../managers/AccountManager","../dev-tools/LinkFinder") as $class)
      require_once(__DIR__."/".$class.".php");

    if(!isset($_SESSION["managerTest"]))
      $_SESSION["managerTest"] = new AccountManager(); ?>
  </head>
  <body>
    <?php // Décommentez les instructions pour pouvoir tester chaque méthode indépendamment des autres !
    /*echo "<h2>Test de connexion Internet pour la VM</h2>";
    echo "<img src=\"https://image.shutterstock.com/image-photo/colorful-flower-on-dark-tropical-600w-721703848.jpg\"></img>"; //*/
    echo "<h2>Attributs de l'objet test</h2>";
    $status = ($_SESSION["managerTest"]->accountIsNotDefined()) ? "aucun compte connu..." : "compte défini !";
    echo "<b>Statut du compte lié à l'objet test :</b> $status<br/>";
    echo "<b>Contenu du cookie de session :</b> ".$_COOKIE["userSession"]."<br/>";
    $_SESSION["searcher"] = new LinkFinder(1);
    $test = $_SESSION["searcher"]->searchFile("cookies");
    echo "<b>Lien relatif du dossier des cookies :</b> $test";

    echo "<h2>Tests des fonctions principales en cours...</h2>";
    echo "<b>Test de la session courante :</b> \"".$_SESSION["test"]."\"<br/>";
    $_SESSION["test"] = "Hello world !";
    echo $doubleSaut;
    $_SESSION["managerTest"]->displayLogIOComponent();
    echo $doubleSaut;
    $_SESSION["managerTest"]->displayInputComponent();
    $_SESSION["managerTest"]->displayErrorsComponent(); //*/ ?>
  </body>
</html>
