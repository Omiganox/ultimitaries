<!DOCTYPE html>
<html>
  <head>
    <title>Page de test des DatabaseParsers !</title>
    <?php require "../../vendor/autoload.php";

    use Ultimitaries\Debug\Tester,
        Ultimitaries\Core\Storages\Blueprints\SQL,
        Ultimitaries\Core\Storages\Blueprints\TextFileGetter,
        Ultimitaries\Core\Parsers\Database\DatabaseParser;
    
    $debug = new Tester;
    $parserTest = DatabaseParser::getInstanceFor(new TextFileGetter("DB_Access.ini",TextFileGetter::PARSER,"ULT-1"));
    $debug->print($parserTest);
    $ult2 = "ULT-2";
    try {
      echo "Création d'un premier objet branché sur la section $ult2 : ";
      $parserTest2 = DatabaseParser::getInstanceFor(new TextFileGetter("DB_Access.ini",TextFileGetter::PARSER,$ult2));
      $debug->print($parserTest2);
      
      //unset($parserTest2); echo "Suppression de cet objet...".BR2;

      echo "Création d'un nouvel objet branché sur la section $ult2 : ";
      $parserTest2 = DatabaseParser::getInstanceFor(new TextFileGetter("DB_Access.ini",TextFileGetter::PARSER,$ult2));
      $debug->print($parserTest2);
    }
    catch(Exception $e) {
      echo $e->getMessage().BR2;
    }
    ?>
  </head>
  <body>
    <?php // Décommentez les instructions pour pouvoir tester chaque méthode indépendamment des autres !
    foreach(array("groupsOfAdmins","test") as $num => $tableName) {
      $testCheck = ($parserTest->checkTableExistence($tableName)) ? "la table \"$tableName\" existe !" : "aucune table \"$tableName\" trouvée...";
      echo "Test de checkTableExistence n°".++$num." : $testCheck".BR1;
    }
    $debug->print($parserTest->getPrimaryOf("test"),prefix:"Test de 'getPrimaryOf' sur la table 'test' : ",br:1);
    $parserTest->readjustAutoIncrement("test");
    echo "Test de generateID : ".$parserTest->generateID("test").BR1;
    $queryStart = "select * from test where id_test = ";

    foreach(array("sans" => "$queryStart 1","avec" => "$queryStart ?") as $key => $query)
      $debug->print(
        $parserTest->execute(($key<=>"sans") == 0 ? new SQL($query) : new SQL($query,[1])),
        prefix:"Test $key mot-clé de substitution : ",
        br:1
      );
    ?>
  </body>
</html>
