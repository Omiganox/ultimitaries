<!DOCTYPE html>
<html>
  <head>
    <title>Page de test des plans de construction !</title>
    <?php require "../../vendor/autoload.php";

    use Ultimitaries\Debug\Tester,
        Ultimitaries\Core\Storages\Blueprints\SQL,
        Ultimitaries\Core\Storages\Blueprints\TextFileDeletion as TFD,
        Ultimitaries\Core\Storages\Blueprints\TextFileGetter as TFG,
        Ultimitaries\Core\Storages\Blueprints\TextFileModification as TFM,
        Ultimitaries\Core\Parsers\Database\Blueprints\DatabaseParser as DP;
    ?>
  </head>
  <body>
    <?php // Décommentez les instructions pour pouvoir tester chaque méthode indépendamment des autres !
    $debug = new Tester;
    $fileTest = "../";
    /*$debug->print(new SB(SB::DB_S,new DP(DP::MySQL,"DB_Access.ini","PDO")));
    $debug->print(new SB(SB::TF_S));
    $debug->print(new SB(SB::TF_S,new TFD($fileTest,TFD::ALL_FILE)));

    $debug->print(new TFG($fileTest,TFG::ALL_CONTENT));
    $test = new TFM($fileTest,TFM::OVERWRITE,"Blabla : 1la\nblargh : aucune répétition"); //*/
    try {
      $test = new SQL("insert into test values(:id,:value)",[[":id" => 1,":value" => 3],[":id" => 1,":value" => 2]]);
      $debug->print($test);
      $debug->print($test->isStatusOk());
      $test->setQuery("insert into test values(".(true ? "?,?,?" : ":id,:key,:value").")");
      $debug->print($test);
      $debug->print($test->isStatusOk());
      $test->setKeywords([
        //[":id" => 1],
        //[":Id" => 1,":kay" => 1.5,":values" => 2],
        //[":id" => 1,":key",":value" => 2],
        [1,2,3]
      ]);
      $debug->print($test);
    }
    catch(InvalidArgumentException $e) {
      echo "Arrêt de l'exécution du script : ".$e->getMessage()."<br>".$e->getTraceAsString().BR2;
    } //*/
    /*$debug->print(new TFD($fileTest,TFD::TARGET,"blargh : aucune répétition"));
    $debug->print(new TFM($fileTest,TFM::APPEND,"\nBlabla : 2li\n\n\nBlabla : 4le\n\nBlabla : 5lo"));
    $debug->print(new TFM($fileTest,TFM::UPDATE,"Blabla : 3lu","Blabla : 2li"));
    $debug->print(new TFG($fileTest,TFG::TARGET,"Blabla",":")); //*/
    ?>
  </body>
</html>
