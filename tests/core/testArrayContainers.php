<!DOCTYPE html>
<html>
  <head>
    <title>Page de test des conteneurs spécialisés !</title>
    <?php require "../../vendor/autoload.php";

    use Ultimitaries\Debug\Tester,
        Ultimitaries\Core\Containers\Arrays\ArrayObjects\ArrayList,
        Ultimitaries\Core\Containers\Arrays\ArrayObjects\Map,
        Ultimitaries\Core\Containers\Arrays\FirstOuts\Queue,
        Ultimitaries\Core\Containers\Arrays\FirstOuts\Stack;
    ?>
  </head>
  <body>
    <?php // Décommentez les instructions pour pouvoir tester chaque méthode indépendamment des autres !
    $debug = new Tester;
    $tab = array("Zero" => false,"masjestuatisation",array("test","récursif"),true,0,"du cul",1,true);

    /** @param  ArrayList|Map $container */
    function sequenceTest(Countable $container,int $index) {
      global $debug;
      echo "Le tableau est-il vide ? ".($container->isEmpty() ? "Oui" : "Non, voici sa taille : {$container->count()}.").BR1;
      echo "Possède-t-il une valeur à l'index $index ? ".($container->hasKey($index) ? "Oui." : "Non.").BR1;
      echo "Possède-t-il une valeur \"false\" ? ".($container->hasValue(false) ? "Oui." : "Non.").BR2;
      $debug->print($container);
    }
    // Test de la classe "Map"
    $mapTest = (new Map($tab,Map::FIRST_LEVEL))->changeLock(true,0,2,5)->changelock(false,2);
    $mapTest["himself"] = $mapTest;
    $debug->print($mapTest->isList());
    $debug->print($mapTest->getArrayCopy(),prefix:"Copie sans verrou : ");
    $debug->print($mapTest->getArrayCopy(true),prefix:"Copie avec verrou : ");

    $debug->print($mapTest->get(1),prefix:"Index 1 :",br:1);
    echo "Index 0 verrouillé ? ".($mapTest->isLocked(0,false) ? "Oui." : "Non.").BR1;
    echo "Index 2 verrouillé ? ".($mapTest->isLocked(2,false) ? "Oui." : "Non.").BR1;
    echo "Index 4 : {$mapTest[4]}".BR2;

    $debug->print($mapTest->array_keys(1),prefix:"Clés correspondant à l'entier '1' : ",br:1);
    $debug->print($mapTest->array_keys(1,true),prefix:"Clés correspondant à l'entier '1' (strict) : ",br:1);
    $debug->print($mapTest->array_keys(),prefix:"Toutes les clés : ");
    sequenceTest($mapTest->changelock(true)->changelock(false,4),3);

    $serial = serialize($mapTest);
    echo "Sérialisation de l'objet : $serial".BR2;
    echo $mapTest->changeLock(false,2);
    echo $mapTest->unsetAll(true,true);
    $mapTest->clear();
    sequenceTest($mapTest,3);
    $mapTest2 = unserialize($serial);
    sequenceTest($mapTest2,3);
    echo $mapTest2->sort(flags: SORT_LOCALE_STRING);
    echo $mapTest2->ksort(flags: SORT_LOCALE_STRING); //*/

    // Test de la classe "ArrayList"
    $enonce = function($o) {
      $enonce = " du tableau : ";

      foreach(array("Début" => "first","Milieu" => "middle","Fin" => "last") as $start => $method) {
        echo $start.$enonce;
        var_dump($o->$method());
        echo BR1;
      }
      echo BR1;
    };
    $listTest = new ArrayList($tab);
    $debug->print($listTest->isList());
    echo $listTest->sort(flags: SORT_LOCALE_STRING);
    $enonce($listTest);

    // Test des insertions/suppresions
    echo $listTest->pop().BR1.$listTest->shift().BR1.$listTest;
    echo $listTest->unshift(1,2,3)->push(4,5,6);
    echo $listTest->unset(3);
    echo $listTest->unsetAll(true,true);
    
    // Test de la conversion "ArrayList <-> Map"
    echo $mapTest2;
    $mapTest = $listTest->toMap();
    echo $mapTest;
    $debug->print($mapTest->isList());
    $debug->print($mapTest);
    
    $listTest = $mapTest->toList();
    echo $listTest;
    $debug->print($listTest); //*/

    // Test des classes "Queue" et "Stack"
    /*$queueTest = new Queue("Un","deux",3);
    $queueTest->modifyLimit(1)->modifyLimit(5);
    $debug->print($queueTest->isLimited(),prefix:"Limite : ",br:1);
    $queueTest(4,5,6);
    echo $queueTest.BR2;
    $queueTest->modifyLimit();
    $debug->print($queueTest->isLimited(),prefix:"Limite : ",br:1);
    $queueTest->pop($a)->push($a,"Trois")->pop($b)->push($queueTest);
    echo "a = $a, b = $b".BR1;
    echo $queueTest.BR2; //*/
    
    $stackTest = new Stack("Un","deux",3);
    $stackTest->modifyLimit(7);
    echo "Limite : ".$stackTest->isLimited().BR1;
    echo $stackTest(4,5,6).BR2;
    echo $stackTest->push($stackTest("deux","Trois")).BR2;
    $stackTest->pop($c);
    echo "c = $c".BR1.$stackTest; ?>
  </body>
</html>
