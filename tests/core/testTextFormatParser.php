<!DOCTYPE html>
<html>
  <head>
    <title>Page de test des DatabaseParsers !</title>
    <?php require "../../vendor/autoload.php";

    use Ultimitaries\Debug\Tester,
        Ultimitaries\Core\Parsers\TextFormats\Factories\TextFormatParser as F_TFP;
    
    $debug = new Tester;
    $fileTest = "DB_Access.ini";
    $fileContent = \file_get_contents($fileTest);
    $iniTest = F_TFP::getInstanceFor($fileTest);
    $jsonTest = F_TFP::getInstanceFor("json");
    $xmlTest = F_TFP::getInstanceFor(F_TFP::XML); ?>
  </head>
  <body>
    <?php // Décommentez les instructions pour pouvoir tester chaque méthode indépendamment des autres !
    $debug->print($fileContent);

    foreach(array($fileContent,$fileTest) as $key => $content) {
      $iniContent = $iniTest->decode($content);
      $debug->print($iniContent,prefix:($key+1).") Décodage INI : ");
    }
    $jsonContent = $jsonTest->encode($iniContent);
    echo "Encodage JSON : ".$jsonContent.BR2;

    if(!file_exists("testParser.json"))
      file_put_contents("testParser.json",$jsonContent);
    $content = $jsonTest->decode($jsonContent);
    $debug->print($content,prefix:"Décodage JSON : ");

    array_unshift($content,"//Pour plus de détails, voir : https://www.php.net/manual/fr/function.parse-ini-file");
    $content["Hola"] = array("test","random","du","cul");
    $iniContent = $iniTest->encode($content);
    $debug->print($iniContent,prefix:"Encodage INI : ");
    
    $content = $iniTest->decode($iniContent);
    $debug->print($content,prefix:"Décodage INI : ",br:1);
    unset($debug);
    ?>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script type="text/javascript">
    window.onload = function() {
      $.get("testParser.json",function(data) {
        console.log(data);
      });
    }
  </script>
  </body>
</html>
