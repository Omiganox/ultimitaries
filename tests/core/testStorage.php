<!DOCTYPE html>
<html>
  <head>
    <title>Page de test des stockeurs externes !</title>
    <?php require "../../vendor/autoload.php";
    
    use Ultimitaries\Debug\Tester,
        Ultimitaries\Core\A_Blueprint,
        Ultimitaries\Core\Storages\Blueprints\SQL,
        Ultimitaries\Core\Storages\Blueprints\TextFileDeletion as TFD,
        Ultimitaries\Core\Storages\Blueprints\TextFileGetter as TFG,
        Ultimitaries\Core\Storages\Blueprints\TextFileModification as TFM,
        Ultimitaries\Core\Storages\DatabaseStorage,
        Ultimitaries\Core\Storages\TextFileStorage,
        Ultimitaries\Core\Storages\Blueprints\TextFileGetter,
        Ultimitaries\Core\Storages\Factories\Storage as F_S;
    ?>
  </head>
  <body>
    <?php // Décommentez les instructions pour pouvoir tester chaque méthode indépendamment des autres !
    $creationOf = function(int|string $type,?A_Blueprint $blueprint=null) {
      return F_S::getInstanceOf($type,$blueprint);
    };
    $textFileTest = !true;
    $debug = new Tester;

    if($textFileTest) {
      $fileTest = "lien/relatif/de/test.txt";
      $getterAllBlueprint = new TFG($fileTest,TFG::ALL_CONTENT);
      echo "Le fichier texte existe-t-il déjà ? ".(file_exists($fileTest) ? "Oui." : "Non.").BR2;
      $type = F_S::TF_S;
      $instanceBlueprint = null;
    }
    else {
      $query = new SQL("create table if not exist test_DS (id_test2 int(1) primary key AUTO_INCREMENT, test_date2 datetime)");
      echo "La requête de création est-elle correcte ? ".($query->isStatusOk() ? "Oui." : "Non.").BR2;
      $type = F_S::DB_S;
      $instanceBlueprint = new TextFileGetter("DB_Access.ini",TextFileGetter::PARSER,"ULT-1");
    }
    $storageTest = $creationOf($type,$instanceBlueprint);

    if($storageTest->canBeUsed()) {
      function printContent($result,int $echo,bool $file=true) {
        global $debug;
        $container = $file ? "du fichier" : "de la table";
        $debug->print($result,callback:"print_r",prefix:match($echo) {
          1 => "Contenu $container : ",
          2 => "Contenu $container mis à jour : ",
          3 => "Information recherchée : "
        });
      }
      if($storageTest instanceof TextFileStorage) {
        $storageTest->delete(new TFD($fileTest,TFD::ALL_FILE));
        echo "Le fichier texte existe-t-il encore ? ".($storageTest->exist($fileTest,false) ? "Oui." : "Non.").BR2;

        $storageTest->create(new TFM($fileTest,TFM::OVERWRITE,explode("\n","Blabla : 1la\nblargh : aucune répétition")));
        printContent($storageTest->get($getterAllBlueprint),1);

        $storageTest->delete(new TFD($fileTest,TFD::TARGET,"blargh : aucune répétition"));
        printContent($storageTest->get($getterAllBlueprint),2);

        $storageTest->add(new TFM($fileTest,TFM::APPEND,"\nBlabla : 2li\n\n\nBlabla : 4le\n\nBlabla : 5lo"));
        printContent($storageTest->get($getterAllBlueprint),1);

        $storageTest->update(new TFM($fileTest,TFM::UPDATE,array("Blabla : 3lu"),array("Blabla : 2li")));
        printContent($storageTest->get($getterAllBlueprint),2);

        printContent(($storageTest->get(new TFG($fileTest,TFG::TARGET,"Blabla",":")))->getArrayCopy(),3);
      }
      else if($storageTest instanceof DatabaseStorage) {
        try {
          $getterAllBlueprint = new SQL("select * from test_DS");
          $id = $storageTest->getDbParser()->generateID("test_DS");
          echo "Nouvelle ID générée pour la table test : $id".BR2;

          $storageTest->add($query->setQuery("insert into test_DS values($id,now())"));
          printContent($storageTest->get($getterAllBlueprint),1,false);

          $storageTest->delete($query->setQuery("delete from test_DS where id_test2 = :id")->setKeywords([[":id" => 3],[":id" => 6]]));
          printContent($storageTest->get($getterAllBlueprint),2,false);

          $storageTest->update($query->setQuery("update test_DS set test_date2 = now() where id_test2 = :id")->setKeywords([":id" => 5]));
          printContent($storageTest->get($getterAllBlueprint),2,false);

          $getterBlueprint = new SQL("select * from test_DS where id_test2 = ?",[[2],[4],[6],[8]]);
          printContent($storageTest->get($getterBlueprint),2,false);
        }
        catch(\InvalidArgumentException $e) {
          echo $e->getMessage().BR1.$e->getTraceAsString().BR2;
        }
      }
    }
    else
      echo "<b>NoStorage is coming !</b>".BR2;
    $debug->print($storageTest,br:1);
    ?>
  </body>
</html>
