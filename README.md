# Ultimitaries
## Spécifications
- Version actuelle du projet : 0.9.1
- Dépendances : >= PHP 8.0
- Appels dynamiques des fichiers gérés par Composer.

## Description générale
Ultimitaries est un framework personnel qui a été créé pour développer des sites web au code source fiable et sécurisé. Il contient une multitude de structures de données et d'outils spécialisés dont l'objectif premier est de faciliter la mise en œuvre d'une programmation orientée objet en PHP côté serveur. Il est exclusif aux versions 8.x du langage.