<?php namespace Ultimitaries\Debug;
/**
 * @version 1.0
 * @category  Outils de débuggage
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2022, Ultimitaries
 */

/** Chaque objet de cette classe configure les environnements de test et fournit des informations utiles aux utilisateurs souhaitant tester et débuguer leurs applications, tel que les messages d'erreurs, le temps d'exécution des scripts et la consommation de mémoire. */
class Tester {
  /** @var  float $executionStart Le temps système enregistré avant le début des tests. */
  private float $executionStart;
  /** @var  float $memoryStart  Le montant de mémoire utilisé avant le début des tests. */
  private float $memoryStart;

  public function __construct() {
    \ini_set("display_errors",1);
    \error_reporting(E_ALL);
    \define("BR1","<br>");
    \define("BR2","<br><br>");
    $this->executionStart = \microtime(true);
    $this->memoryStart = \memory_get_usage();
  }

  /** Retourne le temps d'exécution total et la consommation mémoire du script à la destruction de l'objet. */
  public function __destruct() {
    $scriptMemoryUsage = \round((\memory_get_usage() - $this->memoryStart) / 1024);
    $scriptMemoryPeak = \round((\memory_get_peak_usage()) / 1024);
    $chunkMemoryPeak = \round((\memory_get_peak_usage(true)) / 1024);
    echo BR2."Time execution : ".\number_format(\microtime(true)-$this->executionStart,5)." sec".
         BR1."Used memory (script) : $scriptMemoryUsage / $scriptMemoryPeak KB (".\number_format($scriptMemoryUsage/$scriptMemoryPeak*100,2)."%)".
         BR1."Used memory (chunk) : $scriptMemoryUsage / $chunkMemoryPeak KB (".\number_format($scriptMemoryUsage/$chunkMemoryPeak*100,2)."%)";
  }

  /**
   * Affiche les informations d'une variable.
   * 
   * @param mixed $data La donnée à afficher.
   * @param string  $callback [Optionnel] La fonction à utiliser.
   * @param string  $prefix [Optionnel] Un message à afficher avant les informations essentielles.
   * @param int $br [Optionnel] Le nombre de saut de ligne à mettre.
   */
  public function print(mixed $data,string $callback="var_dump",string $prefix="",int $br=2) {
    if(!empty($prefix))
      echo $prefix;
    \call_user_func($callback,$data);
    echo $br == 2 ? BR2 : BR1;
  }
}