<?php namespace Ultimitaries\Components;
/**
 * @version 5.3.0
 * @category  Utilitaires spécialisés
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

use Ultimitaries\Core\Containers\Arrays\ArrayObjects\Map,
    Ultimitaries\Core\Modules\I_Usage,
    Ultimitaries\Core\Modules\T_StringMethods,
    Ultimitaries\Core\Modules\T_Utils,
    Ultimitaries\Core\Modules\Logs\T_Logs,
    Ultimitaries\Core\Storages\TextFileStorage,
    Ultimitaries\Core\Storages\Blueprints\TextFileGetter as TFG,
    Ultimitaries\Core\Storages\Blueprints\TextFileModification as TFM,
    Ultimitaries\Core\Storages\Factories\Storage as F_S;

/** Les objets de cette classe ont pour rôle de trouver des fichiers dans l'arborescence de l'application et de créer leur lien relatif en fonction du répertoire où ils sont exécutés. S'ils le peuvent, ils sauvegarderont les liens trouvés dans un fichier texte avant d'être détruits ; à la création d'un nouvel objet, ce dernier pourra les récupérer automatiquement et n'effectuera les recherches qu'en cas de besoin. */
final class LinkFinder {
  use T_StringMethods, T_Utils, T_Logs;

  /** Constante de classe renseignant diverses informations fixes et propres à la classe courante. */
  private const DIRS_TO_IGNORE = [".git","Ultimitaries"], SAVE_FILE = __DIR__."/knowRelativeLinks.txt";

  /** @var  array $currentKeywords  Les mot-clés utilisés lors de la dernière recherche d'un lien. Cette propriété permet d'alléger la mémoire consommée lorsque le DFS de la méthode "search" est utilisé. */
  private array $currentKeywords = [];
  /** @var  Map $relativeLinks  L'objet "Map" contenant les liens relatifs trouvés par les objets de la classe courante. */
  private Map $relativeLinks;
  /** @var  I_Usage|TextFileStorage $storage  Un objet de l'ensemble de classes "Storage" pour stocker ou non les données de l'outil dans des supports externes. */
  private I_Usage $storage;

  /**
   * @param ?string  $appRoot  [Optionnel] Le nom du répertoire qui va délimiter le champ de recherche de l'objet ; par défaut, il s'agit du répertoire où l'objet est instancié.
   * @param bool  $saveLinks  [Optionnel] Si faux, les liens trouvés ne seront pas sauvegardés dans le fichier prévu à cet effet.
   */
  public function __construct(private ?string $appRoot=null,bool $saveLinks=true) {
    $this->integrateLogWriter();

    if(!\is_null($appRoot)) {
      $errorLog = "First constructor's argument ";
      $initialLogSize = \strlen($errorLog);

      if(\substr_count($appRoot,"/") >= 1)
        $errorLog .= "can't be a link or the server's root ('/') !";
      else if(\substr_count(\getcwd(),$appRoot) === 0)
        $errorLog .= "isn't a part of executive directory's absolute path !";
      else if(\substr_count(\getcwd(),$appRoot) === 1 && \preg_match("/var|www|html/",$appRoot))
        $errorLog .= "can't be one of main Apache's directories (var|www|html) !";
    }
    if(\strlen($errorLog) > $initialLogSize) {
      $this->createLog(new \InvalidArgumentException($errorLog));
      $this->appRoot = $this->getLastDir(\getcwd());
    }
    $this->storage = F_S::getInstanceOf($saveLinks && \is_writable(__DIR__) ? F_S::TF_S : F_S::N_S);

    if($this->storage->canBeUsed() && $this->storage->exist(self::SAVE_FILE,false)) {
      try {
        $saveFileContent = $this->storage->get(new TFG(self::SAVE_FILE,TFG::ALL_CONTENT))[0];
        $this->relativeLinks = empty($saveFileContent) ? new Map : \unserialize($saveFileContent);
      }
      catch(\Error|\Exception $e) {
        $this->createLog($e);
      }
    }
    if(empty($this->relativeLinks))
      $this->relativeLinks = new Map;
  }

  /** Exécute des instructions supplémentaires lors de la destruction de l'objet. */
  public function __destruct() {
    if($this->storage->canBeUsed() && !$this->relativeLinks->isEmpty())
      $this->storage->create(new TFM(self::SAVE_FILE,TFM::OVERWRITE,\serialize($this->relativeLinks)));
  }

  /**
   * Crée et retourne le lien relatif d'un fichier en fonction du répertoire d'exécution de l'objet.
   *
   * @param string  $fileLink Le lien absolu à transformer en lien relatif.
   */
  private function createRelativeLink(string $fileLink): string {
    $fileLinkParts = \array_slice(\explode("/",$fileLink),\substr_count($fileLink,"/var/www/html/") === 1 ? 4 : 0);
    $cwdLinkParts = \array_slice(\explode("/",\getcwd()),4);

    while(!empty($fileLinkParts) && !empty($cwdLinkParts) && $this->areEquals($fileLinkParts[0],$cwdLinkParts[0])) {
      \array_shift($fileLinkParts);
      \array_shift($cwdLinkParts);
    }
    return str_repeat("../",\count($cwdLinkParts)).(empty($fileLinkParts) ? "" : join("/",$fileLinkParts)).(\is_dir($fileLink) ? "/" : "");
  }

  /**
   * Retourne le nom du dernier dossier d'un lien.
   *
   * @param string  $fileLink Le lien à analyser.
   */
  private function getLastDir(string $fileLink): string {
    $a = \explode("/",\is_dir($fileLink) ? $fileLink : \dirname($fileLink));
    return \end($a);
  }

  /**
   * Trouve et retourne le lien relatif d'un fichier présent dans l'arborescence du site web. La comparaison des arguments est sensible à la casse.
   *
   * @param string  $fileName Le nom complet du fichier à trouver.
   * @param string[]  [Optionnel] $keywords  Le nom des dossiers à trouver obligatoirement dans le lien relatif du fichier cible.
   */
  public function getLinkFor(string $fileName,string ...$keywords): ?string {
    if(!($emptyKeywords = empty($this->currentKeywords = $keywords))) {
      \sort($keywords,SORT_LOCALE_STRING);
      $logKeywords = \join("+",$keywords);
    }
    $fileKey = $this->getLastDir(\getcwd()).($emptyKeywords ? "" : "->$logKeywords")."->$fileName";

    if(empty($fileLink = $this->relativeLinks[$fileKey] ?? null) | !\file_exists($fileLink))
      if(empty($fileLink = $this->search($fileName,\getcwd())))
        $this->createLog(new \Exception("File '$fileName'".($emptyKeywords ? "" : " (keywords : $logKeywords)")." doesn't exist in app !"));
      else
        $this->relativeLinks[$fileKey] = $fileLink;
    return $fileLink;
  }

  /**
   * Trouve et retourne le lien absolu d'un fichier dans la limite du champ de recherche de l'objet. La technique employée est le DFS.
   *
   * @param string  $targetedFile Le nom et l'extension du fichier à trouver.
   * @param string  $currentDirLink  Le lien du répertoire à fouiller.
   * @param bool  $subDir [Optionnel] Si le dossier en train d'être fouillé est un sous-dossier ou non.
   * @param string  $ignored [Optionnel] Le nom du répertoire supplémentaire à ignorer.
   */
  private function search(string $targetedFile,string $currentDirLink,bool $subDir=false,string $ignored=null): ?string {
    $dirContent = (new Map(\array_slice(\scandir($currentDirLink),2)))->getIterator(); // "." et ".." sont retirés
    $ignoredDirs = empty($ignored) ? self::DIRS_TO_IGNORE : \array_merge(self::DIRS_TO_IGNORE,[$ignored]);
    $subDirs = new Map;

    while(($noResult = empty($result)) && $dirContent->valid()) {
      if(!\in_array($fileName = $dirContent->current(),$ignoredDirs)) {
        $fileLink = "$currentDirLink/$fileName";

        if($this->areEquals($fileName,$targetedFile)) {
          if(empty($this->currentKeywords) ?: $this->containsWords($currentDirLink,$this->currentKeywords))
            $result = $this->createRelativeLink($fileLink);
        }
        else if(\is_dir($fileLink))
          if(\is_readable($fileLink))
            $subDirs->push($fileLink);
          else
            $this->createLog(new \LogicException("Repertory '$fileLink' can't be read !"));
      }
      $dirContent->next();
    }
    if($noResult && !$subDirs->isEmpty()) {
      $subIterator = $subDirs->getIterator();

      while(($noResult = empty($result)) && $subIterator->valid()) {
        $result = $this->search($targetedFile,$subIterator->current(),true);
        $subIterator->next();
      }
    }
    if($noResult && !$subDir && !$this->areEquals($this->appRoot,$searchedRepertory = $this->getLastDir($currentDirLink)))
      $result = $this->search($targetedFile,\dirname($currentDirLink),ignored: $searchedRepertory);
    return $result ?? null;
  }
}