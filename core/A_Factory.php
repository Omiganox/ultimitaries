<?php namespace Ultimitaries\Core;
/**
 * @version 0.1.2
 * @category  Fabriques abstraites
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

use Ultimitaries\Core\Modules\T_Abstraction;

/** Fournit les méthodes nécessaires pour implémenter des fabriques pour les ensembles de classes. */
abstract class A_Factory {
  use T_Abstraction;

  /**
   * Crée et retourne l'objet d'un ensemble de classes.
   *
   * @param int|string  $type L'identifiant permettant de créer un nouvel objet.
   * @param ?A_Blueprint  $specs  [Optionnel] Un plan de construction qui fournit des données nécessaires à la construction du nouvel objet.
   */
  abstract public static function getInstanceOf(int|string $type,?A_Blueprint $specs=null);
}
