<?php namespace Ultimitaries\Core\Containers;
/**
 * @version 0.1
 * @category  Conteneurs spécialisés
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2022, Ultimitaries
 */

use Ultimitaries\Core\Modules\T_Utils,
    Ultimitaries\Core\Modules\Logs\T_Logs;

/** Fournit des propriétés ou des méthodes pour tous les conteneurs spécialisés du framework. */
trait T_Container {
  use T_Utils, T_Logs;

  /**
   * Vérifie si la valeur reçue correspond à la référence de l'objet.
   *
   * @param mixed $value  La donnée à contrôler.
   */
  final protected function isNotItself(mixed $value): bool {
    return !$this->areEquals($value,strict:true) ?: $this->createLog(new \LogicException("Object can't be self-inserted into its own 'storage' !"));
  }
}
