<?php namespace Ultimitaries\Core\Containers\Arrays\FirstOuts;
/**
 * @version 0.4
 * @category  Conteneurs spécialisés
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

use Ultimitaries\Core\Containers\Arrays\T_ArrayContainer;

/** Regroupe le tableau et les méthodes nécessaires à la mise en œuvre des structures de type FIFO et LIFO. */
abstract class A_FirstOut implements \Countable {
  use T_ArrayContainer;

  /** 
   * L'argument le plus à droite entre en premier.
   * 
   * @param ?string $callback [Optionnel] La fonction de rappel à utiliser pour l'insertion des données dans "storage".
   * @param array $values [Optionnel] Des valeurs quelconques à insérer.
   */
  public function __construct(string $callback,array $values=[]) {
    $this->integrateLogWriter();
    $this->callback = $callback;
    $this->controlInsertion(\array_reverse($values));
  }

  /**
   * Dépile un élément du tableau depuis l'extrémité droite.
   *
   * @param mixed $element  L'élément dépilé.
   */
  final public function pop(mixed &$element): self {
    $element = \array_pop($this->storage);
    return $this;
  }
}
