<?php namespace Ultimitaries\Core\Containers\Arrays\FirstOuts;
/**
 * @version 1.1
 * @category  Conteneurs spécialisés
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

/** Les objets de cette classe se comportent comme une pile de données (Last In First Out). */
class Stack extends A_FirstOut {
  /** @param  mixed[] $args Des valeurs quelconques. */
  public function __construct(...$args) {
    parent::__construct("array_push",$args);
  }

  /** Retourne le contenu de la pile de l'objet sous la forme d'une chaîne de caractères. */
  public function __toString(): string {
    if($this->isEmpty())
      return "The stack is empty.";
    else {
      $lifo = array();

      foreach($this->storage as $num => $elem)
        $lifo[] = "<u>$num | $elem</u>";
      return "Content of stack :<br>".\join("<br>",\array_reverse($lifo));
    }
  }
}
