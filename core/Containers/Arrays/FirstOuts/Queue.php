<?php namespace Ultimitaries\Core\Containers\Arrays\FirstOuts;
/**
 * @version 1.1
 * @category  Conteneurs spécialisés
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

/** Les objets de cette classe se comportent comme une file de données (First In First Out). */
class Queue extends A_FirstOut {
  /** @param  mixed[] $args Des valeurs quelconques. */
  public function __construct(...$args) {
    parent::__construct("array_unshift",$args);
  }

  /** Retourne le contenu de la file de l'objet sous la forme d'une chaîne de caractères. */
  public function __toString(): string {
    if($this->isEmpty())
      return "The queue is empty.";
    else {
      $fifo = "";

      foreach($this->storage as $elem)
        $fifo .= "$elem > ";
      return "Content of queue : In > {$fifo}Out";
    }
  }
}
