<?php namespace Ultimitaries\Core\Containers\Arrays;
/**
 * @version 0.5
 * @category  Conteneurs spécialisés
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

use Ultimitaries\Core\Containers\Elem,
    Ultimitaries\Core\Containers\T_Container;

/** Fournit des propriétés et des méthodes dédiées aux classes contruites autour de la gestion d'un tableau. */
trait T_ArrayContainer {
  use T_Container;

  /** @var string  $callback La fonction de rappel utilisée pour ajouter des éléments dans "storage". */
  private string $callback;
  /** @var  ?int  $limit  Indique le nombre maximal d'éléments que l'objet peut stocker (illimité, par défaut). */
  private ?int $limit = null;
  /** @var array  $storage Le tableau où sont stockées toutes les données de l'objet. */
  protected array $storage = [];

  /** Alternative à la méthode "push(1+)". */
  public function __invoke(...$args): self {
    return $this->controlInsertion(\array_reverse($args));
  }

  /** Réinitialise la propriété "storage". */
  final public function clear(): self {
    $this->storage = [];
    return $this;
  }

  /**
   * Contrôle les insertions dans le tableau de l'objet. Cette méthode évite un duplicata de code source dans les méthodes d'insertion.
   *
   * @param array $values Les données à insérer dans la propriété "storage".
   * @param callable  $callback La fonction native à utiliser pour l'insertion.
   */
  protected function controlInsertion(array $values,?callable $callback=null): self {
    if(!empty($values))
      foreach($values as $value) {
        if($this->isNotItself($value)) {
          if($this->isFull($value))
            break;
          else
            \call_user_func_array($callback ?? $this->callback,array(&$this->storage,\str_contains(__CLASS__,"A_ArrayObject") ? new Elem($value) : $value));
        }
      }
    return $this;
  }

  final public function count(): int {
    return \count($this->storage);
  }

  /** Vérifie si la propriété "storage" est vide ou non. */
  final public function isEmpty(): bool {
    return empty($this->storage);
  }

  /**
   * Vérifie si l'objet est plein ou non.
   *
   * @param mixed $elem Une valeur quelconque pour préciser l'éventuel log à générer.
   */
  final public function isFull(mixed $elem=null): bool {
    return \is_bool($this->isLimited()) || $this->count() < $this->limit ? false : !$this->createLog(new \LogicException("Can't add ".(\is_null($elem) ? "'NULL'" : "'$elem'")." into object's storage : full capacity reached yet !"));
  }

  /** Vérifie si l'objet possède une limite sur le nombre d'éléments qu'il peut stocker : si vrai, le montant de la limite est retournée. */
  final public function isLimited(): bool|int {
    return empty($this->limit) ? false : $this->limit;
  }

  /**
   * Modifie la limite de stockage de l'objet.
   *
   * @param ?int  $limit  [Optionnel] Si différent de nul, la nouvelle limite ; sinon, le stockage est illimité.
   */
  final public function modifyLimit(?int $limit=null): self {
    if(!empty($limit) && $limit <= 1)
      $this->createLog(new \Exception("An object '".__CLASS__."' should contain at least two elements !"));
    else
      $this->limit = $limit;
    return $this;
  }

  /**
   * Empile un élément dans "storage".
   *
   * @param mixed[] $values  Le ou les élément(s) à empiler.
   */
  final public function push(...$values): self {
    return $this->controlInsertion($values);
  }
}
