<?php namespace Ultimitaries\Core\Containers\Arrays\ArrayObjects;
/**
 * @version 2.1
 * @category  Conteneurs spécialisés
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

use Ultimitaries\Core\Containers\Exceptions\LockedDataException;

/** Les objets de cette classe se comportent comme des listes : leurs clés ne peuvent être que des entiers positifs. */
class ArrayList extends A_ArrayObject {
  /** Avant de passer le premier argument au constructeur parent, on réindexe toutes ses clés. */
  public function __construct(array $storage=[],int $recursive=self::DEFAULT) {
    parent::__construct(\array_values($storage),$recursive);
  }

  /** Retourne le premier élément de "storage". */
  final public function first(): mixed {
    if(!$this->isEmpty())
      return $this->get(0);
  }

  final public function isList(): bool {
    return true;
  }

  /** Retourne le dernier élément de "storage". */
  final public function last(): mixed {
    if(!$this->isEmpty())
      return $this->get($this->array_key_last());
  }

 /** Retourne l'élément présent au milieu de "storage". Si sa taille est paire, les deux éléments centraux sont retournés. */
  final public function middle(): mixed {
    if(!$this->isEmpty()) {
      $halfSize = $this->count() / 2;
      return ($this->count()%2 !== 0) ? $this[$halfSize] : array($this[$halfSize-1],$this[$halfSize]);
    }
  }

  /** Réécriture de la méthode pour empêcher l'insertion de clés non entières dans "storage". */
  final public function offsetSet(mixed $offset,mixed $value): void {
    if(!\is_int($offset))
      $this->createLog(new \InvalidArgumentException("Named keys are forbidden in 'ArrayList' objects !"));
    parent::offsetSet($offset,$value);
  }

  /** Dépile le dernier élément de "storage". */
  final public function pop(): mixed {
    if($this->isLocked($this->array_key_last()))
      throw new LockedDataException("Last storage's element can't be dropped !");
    return (\array_pop($this->storage))->getData();
  }

  /** Dépile le premier élément de "storage". */
  final public function shift(): mixed {
    if($this->isLocked(0))
      throw new LockedDataException("First storage's element can't be dropped !");
    return (\array_shift($this->storage))->getData();
  }

  /**
   * Trie les valeurs de "storage" ; seule la base est affectée et les clés sont automatiquement réindexées.
   * 
   * @param bool  $ascending  [Optionnel] Si faux, l'ordre du tri sera décroissant.
   * @param int $flags  [Optionnel] Une ou plusieurs constantes PHP propres au tri des tableaux.
   * 
   * @link  https://www.php.net/manual/fr/function.sort.php
   */
  final public function sort(bool $ascending=true,int $flags=SORT_LOCALE_STRING,string $prefix=""): self {
    return parent::sort($ascending,$flags,$prefix);
  }

  /** Crée un nouvel objet "Map" à partir de l'objet courant. */
  public function toMap(): Map {
    $map = new Map();
    $map->exchangeArray($this->storage);
    return $map;
  }

  /** Réécriture de la méthode parente pour réindexer automatiquement les clés après la suppression. */
  final public function unset(int|string $key): self {
    if($this->hasKey($key) && !$this->isLocked($key)) {
      while($key < $this->count()-1)
        $this->storage[$key++] = $this->storage[$key];
      unset($this->storage[$key]);
    }
    return $this;
  }

  final public function unsetAll(mixed $target,bool $strict=false): self {
    $initialSize = $this->count();

    foreach($this->getIterator() as $key => $value) {
      if($this->count() < $initialSize)
        $key -= 1;
      if($this->areEquals($target,$value,$strict) && !$this->isLocked($key))
        $this->unset($key);
    }
    return $this;
  }

  /**
   * Insère un ou plusieurs élément(s) au début de "storage" (de gauche à droite).
   *
   * @param mixed[] $value  La ou les valeurs à ajouter au début du tableau.
   */
  final public function unshift(...$values): self {
    return $this->controlInsertion(\array_reverse($values),"array_unshift");
  }

  /**
   * Trie les valeurs de "storage" avec une fonction de tri personnalisée ; seule la base est affectée et les clés sont automatiquement réindexées.
   * 
   * @param callable  $callback Une fonction de rappel.
   * 
   * @link  https://www.php.net/manual/fr/function.usort.php
   */
  final public function usort(callable $callback): self {
    \usort($this->storage,$callback);
    return $this;
  }
}