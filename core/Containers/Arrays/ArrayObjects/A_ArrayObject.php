<?php namespace Ultimitaries\Core\Containers\Arrays\ArrayObjects;
/**
 * @version 0.4
 * @category  Conteneurs spécialisés
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

use Ultimitaries\Core\Containers\Elem,
    Ultimitaries\Core\Containers\I_LockSystem,
    Ultimitaries\Core\Containers\Arrays\T_ArrayContainer,
    Ultimitaries\Core\Containers\Exceptions\LockedDataException;

/** Redéfinition personnalisée de la classe native "ArrayObject" dans l'espace de nom du framework. Par rapport à l'original, cette version est abstraite et intègre directement le système de verrouillage développé dans une ancienne version de la classe "Map". */
abstract class A_ArrayObject implements \ArrayAccess, \Countable, I_LockSystem, \IteratorAggregate {
  use T_ArrayContainer;

  /**
   * Constante de classe indiquant le niveau de récursivité pour la création de nouveaux objets "A_ArrayObject" dans les méthodes.
   *  - DEFAULT : valeur par défaut, aucune récursivité appliquée.
   *  - FIRST_LEVEL : seuls les tableaux contenus dans le tableau renseigné seront convertis en objets "A_ArrayObject" ; leurs contenus ne sont pas affectés.
   *  - SECOND_LEVEL : identique à FIRST_LEVEL, sauf que le contenu des tableaux imbriqués est aussi vérifié (s'ils contiennent eux-mêmes des tableaux, ils ne seront pas affectés).
   *  - FULL : tous les tableaux imbriqués seront convertis en objets "A_ArrayObject".
   */
  const DEFAULT = 0, FIRST_LEVEL = 1, SECOND_LEVEL = 2, FULL = 3;

  /** @var  string  $iterator Le nom de la classe à utiliser pour générer un nouvel itérateur. */
  private string $iterator = "ArrayIterator";

  /**
   * @param array $storage  Le tableau qui contient toutes les informations stockées.
   * @param int $recursive  [Optionnel] Le niveau de récursivité à appliquer (DEFAULT|FIRST_LEVEL|SECOND_LEVEL|FULL).
   */
  public function __construct(array $storage=[],int $recursive=self::DEFAULT) {
    $this->integrateLogWriter();
    $this->callback = "array_push";

    foreach($storage as $key => $value) {
      if(\is_array($value) && $recursive !== self::DEFAULT)
        $this->storage[$key] = match($recursive) {
          self::FIRST_LEVEL => new Elem(new static($value)),
          self::SECOND_LEVEL => new Elem(new static($value,self::FIRST_LEVEL)),
          self::FULL => new Elem(new static($value,self::FULL))
        };
      else
        $this->storage[$key] = $value instanceof Elem ? $value : new Elem($value);
    }
  }

  /**
   * Cette implémentation de "__call()" permet d'utiliser les fonctions PHP prévues pour les tableaux. Le premier paramètre de la fonction est automatiquement renseigné lors de l'appel. Vous pouvez les appeler de cette façon : $debutTableau = $votreInstance->array_key_first();
   *
   * @param string  $functionName Le nom de la fonction PHP à appeler.
   * @param array  $args  Les arguments liés à la fonction appelée.
   */
  public function __call(string $functionName,array $args): mixed {
    if(!\is_callable($functionName) || \substr_compare($functionName,"array_",0,6))
      return $this->createLog(new \BadMethodCallException(__CLASS__."->$functionName"));
    return \call_user_func_array($functionName,\array_merge(array($this->getArrayCopy()),$args));
  }

  /** Retourne une version simplifiée de l'objet depuis la fonction native "var_dump()". */
  public function __debugInfo(): array {
    return ["arraySize" => $this->count(),"logWriter" => \is_null($this->logWriter) ? "none" : "initialized"];
  }

  /** En cas de sérialisation, seule la propriété "storage" est exportée. */
  public function __serialize(): array {
    $serializedElems = array();

    foreach($this->storage as $key => $elem)
      $serializedElems[$key] = \serialize($elem);
    return ["storage" => $serializedElems];
  }

  /** Retourne le contenu de "storage" sous la forme d'une liste textuelle. */
  public function __toString(): string {
    $dataType = function($value) {
      if(\is_array($value))
        return "Array";
      else if(\is_bool($value))
        return $value ? "True" : "False";
      else if(\is_object($value))
        return $value instanceof Elem ? $value->getData() : "Object ".\get_class($value);
      return $value;
    };
    $string = "";

    foreach($this->storage as $key => $elem)
      $string .= "$key => ".$dataType($elem->getData()).($elem->getLock() ? " (verrouillé)" : "")."<br>";
    return "$string<br>";
  }

  /**
   * En cas de désérialisation, toutes les propriétés sont rechargées.
   *
   * @param array $data  Les données sérialisées d'un autre objet "ArrayObject".
   */
  public function __unserialize(array $data): void {
    if(!isset($this->logWriter) || \is_null($this->logWriter))
      $this->integrateLogWriter();
    foreach($data["storage"] as $key => $serial)
      $this->storage[$key] = \unserialize($serial);
  }

  final public function changelock(bool $lock,...$keys): self {
    foreach(empty($keys) ? $this->array_keys() : $keys as $key)
      $this->storage[$key]->setLock($lock);
    return $this;
  }

  /**
   * Échange le tableau de l'objet par un autre. L'original est retourné sous la forme d'un tableau simple tandis que son remplaçant adopte le système de verrouillage.
   *
   * @param array $newArray Le tableau remplaçant.
   * @param bool  $lock [Optionnel] Si vrai, le verrou de chaque donnée du tableau sortant est conservé.
   * @param int $recursive  [Optionnel] Le niveau de récursivité à appliquer (DEFAULT|FIRST_LEVEL|SECOND_LEVEL|FULL).
   */
  public function exchangeArray(array $newArray,bool $lock=false,int $recursive=self::DEFAULT): array {
    $original = $this->getArrayCopy($lock);
    $this->clear();

    if($newArray instanceof self)
      $this->storage = $newArray;
    else {
      foreach($newArray as $key => $value)
        $this->storage[$key] = $value instanceof Elem ? $value : (\is_array($value) ? new Elem(new static($value,$recursive)) : new Elem($value));
    }
    return $original;
  }

  /**
   * Retourne la donnée associée à la clé renseignée.
   *
   * @param int|string  $key  La clé à renseigner pour obtenir la valeur souhaitée.
   * @param bool  $withLock [Optionnel] Si vrai, l'objet "Elem" contenant la valeur sera retourné.
   */
  final public function get(int|string $key,bool $withLock=false): mixed {
    return $this->hasKey($key) ? ($withLock ? $this->storage[$key] : $this->storage[$key]->getData()) : null;
  }

  /**
   * Retourne une copie de la propriété "storage".
   *
   * @param bool  $lock [Optionnel] Si vrai, le verrou de chaque donnée est conservé.
   */
  final public function getArrayCopy(bool $lock=false): array {
    if($lock)
      return $this->storage;
    else {
      $copy = array();

      foreach($this->storage as $key => $elem) {
        $copy[$key] = $elem->getData();

        if($copy[$key] instanceof A_ArrayObject)
          $copy[$key] = $copy[$key]->getArrayCopy();
      }
      return $copy;
    }
  }

  final public function getIterator(): \Iterator {
    return new $this->iterator($this->getArrayCopy());
  }

  /**
   * Vérifie si "storage" contient la clé recherchée ou non. La recherche n'est effectuée qu'à la base.
   *
   * @param int|string  $key  La clé dont l'existence est à vérifier.
   */
  final public function hasKey(int|string $key): bool {
    return \array_key_exists($key,$this->storage);
  }

  /**
   * Vérifie si le tableau de l'objet possède une valeur strictement identique au paramètre renseigné.
   *
   * @param mixed $value  Une variable quelconque.
   * @param bool  $strict [Optionnel] Si vrai, la comparaison des valeurs est stricte.
   */
  final public function hasValue(mixed $value,bool $strict=false): bool {
    return \in_array($value,$this->getArrayCopy(),$strict);
  }

  final public function isLocked(mixed $key,bool $notify=true): bool {
    if($this->storage[$key]->getLock()) {
      if($notify)
        $this->createLog(new LockedDataException("Data at index '$key' is locked !"));
      return true;
    }
    return false;
  }

  /** Vérifie si "storage" peut être considéré comme une liste ou non (pas de clés non entières qui se suivent à partir de 0). */
  public function isList(): bool {
    return $this->array_is_list($this->storage);
  }

  final public function offsetExists(mixed $offset): bool {
    return $this->hasKey($offset);
  }

  final public function offsetGet(mixed $offset): mixed {
    return $this->get($offset);
  }

  public function offsetSet(mixed $offset,mixed $value): void {
    if(\is_null($offset))
      $this->push($value);
    else
      $this->set($offset,$value);
  }

  final public function offsetUnset(mixed $offset): void {
    $this->unset($offset);
  }

  /**
   * Ajoute ou modifie une association dans "storage".
   *
   * @param int|string  $key  La clé liée à la valeur ciblée.
   * @param mixed $value  Une valeur quelconque.
   */
  final public function set(int|string $key,mixed $value) {
    if($this->hasKey($key)) {
      if(!$this->isLocked($key))
        $this->storage[$key]->setData($value);
    }
    else if($this->isNotItself($value) && !$this->isFull($value))
      $this->storage[$key] = new Elem($value);
  }

  /**
   * Charge une nouvelle classe "\Iterator" pour la génération des itérateurs.
   *
   * @param callable  $className  Le nom de l'itérateur à utiliser.
   */
  final public function setIterator(callable $className): self {
    $this->iterator = $className;
    return $this;
  }

  /**
   * Trie les valeurs de "storage" ; seule la base est affectée.
   *
   * @param bool  $ascending  [Optionnel] Si faux, l'ordre du tri sera décroissant.
   * @param int $flags  [Optionnel] Une ou plusieurs constantes PHP propres au tri des tableaux.
   * @param string  $prefix [Optionnel] Le préfixe pour déterminer quelles fonctions natives de tri doivent être utilisées.
   * 
   * @link  https://www.php.net/manual/fr/function.asort.php
   */
  public function sort(bool $ascending=true,int $flags=SORT_REGULAR,string $prefix="a"): self {
    \call_user_func_array($prefix.($ascending ? "sort" : "rsort"),array(&$this->storage,$flags));
    return $this;
  }

  /**
   * Efface la donnée associée à la clé renseignée (si elle n'est pas verrouillée).
   *
   * @param int|string  $key La clé à renseigner pour supprimer la donnée souhaitée.
   */
  public function unset(int|string $key): self {
    if(!$this->isLocked($key))
      unset($this->storage[$key]);
    return $this;
  }

  /**
   * Retire toutes les occurrences non verrouillées d'une donnée dans "storage".
   *
   * @param mixed $target La variable à supprimer.
   * @param bool  $strict [Optionnel] Si vrai, la comparaison stricte est activée.
   */
  abstract public function unsetAll(mixed $target,bool $strict=false): self;

  /**
   * Trie les clés de "storage" avec une fonction de tri personnalisée ; seule la base est affectée.
   *
   * @param callable  $callback Une fonction de rappel.
   * 
   * @link  https://www.php.net/manual/fr/function.uksort.php
   */
  final public function uksort(callable $callback): self {
    \uksort($this->storage,$callback);
    return $this;
  }

  /**
   * Trie les valeurs de "storage" avec une fonction de tri personnalisée ; seule la base est affectée.
   *
   * @param callable  $callback Une fonction de rappel.
   * 
   * @link  https://www.php.net/manual/fr/function.uasort.php
   */
  public function usort(callable $callback): self {
    \uasort($this->storage,$callback);
    return $this;
  }
}