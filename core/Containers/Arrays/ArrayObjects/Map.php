<?php namespace Ultimitaries\Core\Containers\Arrays\ArrayObjects;
/**
 * @version 2.0.2
 * @category  Conteneurs spécialisés
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

/** Les objets de cette classe se comportent comme un tableau associatif. */
class Map extends A_ArrayObject {
  /**
   * Trie les clés du tableau de l'objet dans l'ordre croissant ou décroissant. Le tri s'effectue uniquement sur le premier niveau.
   * 
   * @param bool  $ascending  [Optionnel] Si faux, l'ordre du tri sera décroissant.
   * @param int $flags  [Optionnel] Une ou plusieurs constantes PHP propres au tri des tableaux.
   * 
   * @link  https://www.php.net/manual/fr/function.ksort.php
   */
  public function ksort(bool $ascending=true,int $flags=SORT_REGULAR): self {
    return parent::sort($ascending,$flags,"k");
  }

  /** Crée un nouvel objet "ArrayList" à partir de l'objet courant (les clés sont automatiquement réindexées). */
  public function toList(): ArrayList {
    $arrayList = new ArrayList();
    $arrayList->exchangeArray($this->storage);
    return $arrayList;
  }

  final public function unsetAll(mixed $target,bool $strict=false): self {
    foreach($this->getIterator() as $key => $value)
      if($this->areEquals($target,$value,$strict) && !$this->isLocked($key))
        $this->unset($key);
    return $this;
  }
}