<?php namespace Ultimitaries\Core\Containers;
/**
 * @version 0.3
 * 
 * @category  Conteneurs spécialisés
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

/** Les classes qui implémentent cette interface pourront utiliser efficacement les objets de la classe "Elem" grâce aux prototypes de méthodes qu'elle fournit. */
interface I_LockSystem {
  /** 
   * Modifie le verrou des données associées aux index fournis en paramètre. Si aucun index n'est fourni, toutes les données seront affectées.
   * 
   * @param bool  $lock Si vrai, les données cibles seront verrouillées.
   * @param int|string[]  $keys Les index des données à dé-verrouiller.
   */
  function changelock(bool $lock,...$keys);

  /**
   * Vérifie si une donnée est verrouillée ou non. L'opération s'effectue par le biais de son index.
   *
   * @param mixed $key  L'index à vérifier.
   * @param bool  $notify [Optionnel] Si faux, le log d'erreur lié à la méthode ne sera pas généré.
   */
  function isLocked(mixed $key,bool $notify=true): bool;
}