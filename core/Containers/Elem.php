<?php namespace Ultimitaries\Core\Containers;
/**
 * @version 1.2
 * 
 * @category  Conteneurs spécialisés
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

/** Les objets de cette classe contiennent une donnée quelconque qui peut être verrouillée par le biais de l'interface "I_LockSystem". */
final class Elem {
  /** @var  mixed $data La donnée à contenir. */
  private mixed $data;
  /** @var  bool  $lock Le verrou associé à la donnée de l'objet : quand il est actif, aucune modification n'est autorisée. */
  private bool $lock = false;

  /** @param mixed $data Une donnée quelconque. */
  public function __construct(mixed $data) {
    $this->data = $data;
  }

  /** Retourne une version simplifiée de l'objet depuis la fonction native "var_dump()". */
  public function __debugInfo(): array {
    return ["data" => $this->data,"lock" => $this->lock];
  }

  /** En cas de sérialisation, seule la propriété "storage" est exportée. */
  public function __serialize(): array {
    return $this->__debugInfo();
  }

  /** Retourne l'objet sous la forme d'une association donnée/état du verrou. */
  public function __toString(): string {
    return $this->data." : verrou ".($this->lock ? "actif" : "inactif")."<br>";
  }

  /**
   * En cas de désérialisation, toutes les propriétés sont rechargées.
   *
   * @param array $data  Les données sérialisées d'un autre objet "ArrayObject". */
  
  public function __unserialize(array $data): void {
    $this->data = $data["data"];
    $this->lock = $data["lock"];
  }

  /** Getter de la propriété "data". */
  public function getData(): mixed {
    return $this->data;
  }

  /**
   * Setter de la propriété "data".
   * 
   * @param mixed $data La nouvelle donnée à enregistrer. */
  
  public function setData(mixed $data): void {
    $this->data = $data;
  }

  /** Getter de la propriété "lock". */
  public function getLock(): bool {
    return $this->lock;
  }

  /**
   * Setter de la propriété "lock".
   * 
   * @param bool  $lock Le nouvel état du verrou.
   */
  public function setLock(bool $lock): void {
    $this->lock = $lock;
  }
}
