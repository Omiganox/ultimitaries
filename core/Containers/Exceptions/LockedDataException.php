<?php namespace Ultimitaries\Core\Containers\Exceptions;
/**
 * @version 0.3
 * @category  Exceptions personnalisées
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

/** Levée dans le cas où l'utilisateur cherche à modifier ou à supprimer un élément alors que celui-ci est verrouillé. */
final class LockedDataException extends \Exception {}