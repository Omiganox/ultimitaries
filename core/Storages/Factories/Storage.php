<?php namespace Ultimitaries\Core\Storages\Factories;
/**
 * @version 0.5
 * @category  Fabriques abstraites
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

use Ultimitaries\Core\A_Blueprint,
    Ultimitaries\Core\A_Factory,
    Ultimitaries\Core\Modules\I_Usage,
    Ultimitaries\Core\Storages\DatabaseStorage,
    Ultimitaries\Core\Storages\EncryptedStorage,
    Ultimitaries\Core\Storages\TextFileStorage,
    Ultimitaries\Core\Storages\NoStorage,
    Ultimitaries\Core\Storages\Blueprints\TextFileGetter;

/** Crée des objets "A_Storage" à partir du plan de construction associé. */
final class Storage extends A_Factory {
  const N_S = 0, DB_S = 10, DB_ES = 11, TF_S = 20, TF_ES = 21;

  private function __construct() {}
  private function __clone() {}

  /**
   * @param int|string  $type Une instance "A_Storage" peut être créé grâce aux constantes suivantes :
   *  - DB_S : un objet "DatabaseStorage" pour le stockage des informations dans une base de données ;
   *  - DB_ES : identique à DB_S, plus une encapsulation dans un objet "EncryptedStorage" qui se charge de dé-crypter les données ;
   *  - TF_S : un objet "TextFileStorage" pour le stockage des informations dans des fichiers texte ;
   *  - TF_ES : identique à TF_S, plus une encapsulation dans un objet "EncryptedStorage" qui se charge de dé-crypter les données.
   */
  public static function getInstanceOf(int|string $type,?A_Blueprint $specs=null): I_Usage {
    if(!\is_null($specs) && !($specs instanceof TextFileGetter))
      throw new \InvalidArgumentException("Object 'TextFileGetter' expected, '".\get_class($specs)."' received instead !");
    return match($type) {
      self::DB_ES => new EncryptedStorage(new DatabaseStorage($specs)),
      self::DB_S => new DatabaseStorage($specs),
      self::TF_ES => new EncryptedStorage(TextFileStorage::getInstance()),
      self::TF_S => TextFileStorage::getInstance(),
      default => NoStorage::getInstance()
    };
  }
}
