<?php namespace Ultimitaries\Core\Storages;
/**
 * @version 1.1
 * @category  Stockage externe des données
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

use Ultimitaries\Core\Modules\I_Usage,
    Ultimitaries\Core\Modules\T_SimpleSingleton;

/** Quand le stockage des données d'un composant n'est pas envisagé ou possible, l'instance unique de cette classe est appelée pour empêcher les opérations de sauvegarde ou de chargement desdites données. */
final class NoStorage implements I_Usage {
  use T_SimpleSingleton;

  public function canBeUsed(): bool {
    return false;
  }
}
