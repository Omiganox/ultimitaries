<?php namespace Ultimitaries\Core\Storages;
/**
 * @version 1.2
 * @category  Stockage externe des données
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

use Ultimitaries\Core\Containers\Arrays\ArrayObjects\ArrayList,
    Ultimitaries\Core\Containers\Arrays\ArrayObjects\Map,
    Ultimitaries\Core\Modules\T_SimpleSingleton,
    Ultimitaries\Core\Parsers\TextFormats\Factories\TextFormatParser,
    Ultimitaries\Core\Storages\Blueprints\A_TextFile,
    Ultimitaries\Core\Storages\Blueprints\TextFileDeletion as TFD,
    Ultimitaries\Core\Storages\Blueprints\TextFileGetter as TFG,
    Ultimitaries\Core\Storages\Blueprints\TextFileModification as TFM;

/** Les objets de cette classe sauvegardent, chargent ou modifient les données d'un composant dans des fichiers texte. */
final class TextFileStorage extends A_Storage {
  use T_SimpleSingleton;

  /**
   * Ajoute, réécrit ou met à jour des données dans un fichier texte.
   *
   * @param TFM $blueprint  Plan de construction contenant les informations nécessaires aux opérations de la méthode.
   */
  public function add(TFM $blueprint): self {
    $fileLink = $this->checkThis($blueprint);

    if($this->exist($fileLink)) {
      if(\is_writable($fileLink)) {
        $newContent = $blueprint->getNewFileContent();

        if(\is_string($newContent))
          $newContent = $this->mergeSubsequentDuplicates(trim($newContent,"\n"),"\n");
        else {
          foreach($newContent as $num => $line) {
            if(empty($line))
              unset($newContent[$num]);
            else
              $newContent[$num] = $this->mergeSubsequentDuplicates(trim($line,"\n"),"\n");
          }
          $newContent = join("\n",$newContent);
        }
        $flags = match($blueprint->getOperationMode()) {
          TFM::APPEND => FILE_APPEND|LOCK_EX,
          TFM::OVERWRITE => LOCK_EX,
          TFM::UPDATE => $this->update($blueprint)
        };
        if(!$this->areEquals($flags,strict:true))
          \file_put_contents($fileLink,$newContent,$flags);
      }
      else
        $this->sendLog();
    }
    return $this;
  }

  /**
   * Valide les plans de construction avant leur utilisation.
   * 
   * @param A_TextFile  $blueprint  Le plan de construction à vérifier.
   */
  protected function checkThis(A_TextFile $blueprint): ?string {
    if($blueprint->isOK())
      return $blueprint->getTextFileLink();
    $this->createLog(new \InvalidArgumentException("Object '".\get_class($blueprint)."'is invalid !"));
    return null;
  }

  /**
   * Crée un nouveau fichier texte selon les spécifications du plan de construction.
   *
   * @param TFM $blueprint  Ledit plan de construction.
   */
  public function create(TFM $blueprint): self {
    $fileLink = $this->checkThis($blueprint);

    if(!$this->exist($fileLink,false)) {
      $apacheRoot = "/var/www/html/";
      $contains = \str_contains($fileLink,$apacheRoot); // Cas ? absolu : relatif
      $link = $contains ? \substr($fileLink,\strlen($apacheRoot)) : $fileLink;
      $rebuildLink = $contains ? $apacheRoot : "";

      foreach(\explode("/",\pathinfo($link,PATHINFO_DIRNAME)) as $dir) {
        $rebuildLink .= "$dir/";

        if(!$this->areEquals($dir,"..") && !$this->exist($rebuildLink,false)) {
          \mkdir($rebuildLink);
          \chmod($rebuildLink,0707);
        }
      }
      \fclose(\fopen($fileLink,"w")); // Création du fichier
      \chmod($fileLink,0707);
    }
    if(!is_null($blueprint->getNewFileContent()))
      $this->add($blueprint);
    return $this;
  }

  /**
   * Supprime des données dans un fichier texte.
   *
   * @param TFD $blueprint  Plan de construction contenant les informations nécessaires aux opérations de la méthode.
   */
  public function delete(TFD $blueprint): self {
    $fileLink = $this->checkThis($blueprint);

    if($this->exist($fileLink)) {
      if(\is_writable($fileLink))
        match($blueprint->getOperationMode()) {
          TFD::ALL_CONTENT => \file_put_contents($fileLink,null,LOCK_EX),
          TFD::ALL_FILE => \unlink($fileLink),
          TFD::TARGET => \file_put_contents($fileLink,\str_replace($blueprint->getTarget(),"",\file_get_contents($fileLink)),LOCK_EX)
        };
      else
        $this->sendLog();
    }
    return $this;
  }

  public function exist(string $targetedContainer,bool $exceptionRaise=true): bool {
    $fileExistence = \file_exists($targetedContainer);
    return $exceptionRaise ? ($fileExistence ?: parent::exist("File '$targetedContainer'")) : $fileExistence;
  }

  /**
   * Récupère des informations dans le fichier texte.
   *
   * @param TFG $blueprint  Plan de construction contenant les informations nécessaires aux opérations de la méthode.
   */
  public function get(TFG $blueprint): mixed {
    $fileLink = $this->checkThis($blueprint);

    if($this->exist($fileLink)) {
      if(\is_readable($fileLink)) {
        $flags = FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES;

        switch($blueprint->getOperationMode()) {
          case TFG::ALL_CONTENT:
            return \file($fileLink,$flags);
          case TFG::PARSER:
            $allSections = new Map((TextFormatParser::getInstanceFor(\pathinfo($fileLink,PATHINFO_EXTENSION)))->decode($fileLink),Map::FULL);
            $wantedSection = $blueprint->getTarget();
            return (!\is_null($wantedSection) && $allSections->hasKey($wantedSection)) ? $allSections[$wantedSection] : $allSections;
          case TFG::TARGET:
            $foundValues = new ArrayList;

            foreach(\file($fileLink,$flags) as $line) {
              $lineParts = new ArrayList(\explode($blueprint->getSplitter(),$line));

              if($this->areEquals(\trim($lineParts->first()),$blueprint->getTarget()))
                $foundValues(\trim($lineParts->last()));
            }
            return $foundValues;
        }
      }
      else
        $this->sendLog("read");
    }
    return null;
  }

  /** @param string  $word [Optionnel] Le mot-clé du log. */
  private function sendLog(string $word="modified"): void {
    $this->createLog(new \LogicException("Targeted file can't be $word (insufficient rights) !"));
  }

  /**
   * Met à jour des données dans un fichier texte.
   *
   * @param TFM $blueprint  Plan de construction contenant les informations nécessaires aux opérations de la méthode.
   */
  public function update(TFM $blueprint): self {
    $fileLink = $this->checkThis($blueprint);

    if($this->exist($fileLink)) {
      if(\is_writable($fileLink)) {
        if($blueprint->getOperationMode() === TFM::UPDATE)
          \file_put_contents(
            $fileLink,
            \str_replace($blueprint->getOldFileContent(),$blueprint->getNewFileContent(),\file_get_contents($fileLink)),
            LOCK_EX
          );
        else
          $this->add($blueprint);
      }
      else
        $this->sendLog();
    }
    return $this;
  }
}