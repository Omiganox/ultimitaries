<?php namespace Ultimitaries\Core\Storages;
/**
 * @version 0.5
 * @category  Stockage externe des données
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

use Ultimitaries\Core\Modules\I_Usage,
    Ultimitaries\Core\Modules\T_Abstraction,
    Ultimitaries\Core\Modules\T_StringMethods,
    Ultimitaries\Core\Modules\Logs\T_Logs;

/** Fournit les prototypes nécessaires à l'implémentation des divers stockeurs de données du framework. */
abstract class A_Storage implements I_Usage {
  use T_Abstraction, T_StringMethods, T_Logs;

  /** Constructeur de la classe courante. */
  public function __construct() {
    $this->integrateLogWriter();
    $this->implementsMethods("add","checkThis","create","delete","get","update");
  }

  /** @see  I_StorageUsage::canBeUsed() */
  final public function canBeUsed(): bool {
    return true;
  }

  /**
   * Vérifie si le conteneur existe.
   *
   * @param string  $targetedContainer Dans la classe courante, le nom du conteneur à vérifier ; dans la classe mère, le message de l'exception à remonter.
   * @param bool  $exceptionRaise [Optionnel] Indique si l'exception doit être levée lors de l'appel de la méthode.
   */
  public function exist(string $targetedContainer,bool $exceptionRaise=true): bool {
    if($exceptionRaise)
      $this->createLog(new \LogicException($targetedContainer." doesn't exist !"));
    return false;
  }
}
