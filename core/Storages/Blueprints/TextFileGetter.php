<?php namespace Ultimitaries\Core\Storages\Blueprints;
/**
 * @version 1.0.2
 * @category  Plans de construction
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

/** Chaque objet de cette classe regroupe les arguments nécessaires à la récupération partielle ou complète du contenu d'un fichier texte. */
final class TextFileGetter extends A_TextFile {
  /** Constantes publiques qui permettent de définir le mode de sélection. */
  const ALL_CONTENT = 1, PARSER = 2, TARGET = 3;

  /**
   * Constructeur de la classe courante.
   *
   * @param string  $link Le lien absolu ou relatif d'un fichier texte.
   * @param int $mode  Trois opérations sur les fichiers ciblés sont possibles grâce aux constantes suivantes :
   *  - ALL_CONTENT : récupère l'intégralité du contenu du fichier ;
   *  - PARSER : identique à "ALL_CONTENT", mais adapté pour les fichiers parsés (https://www.php.net/manual/fr/function.parse-ini-file) ;
   *  - TARGET : récupère des éléments précis dans le fichier grâce aux deux derniers paramètres.
   * @param string  $target [Optionnel] Mot-clé indiquant quelle ligne doit être sélectionnée.
   * @param string  $splitter [Optionnel] Caractères permettant la séparation de données regroupées selon un format précis.
   */
  public function __construct(string $link,int $mode,private ?string $target=null,private ?string $splitter=null) {
    parent::__construct($link,$mode);
  }

  /** Getter de la propriété "target". */
  public function getTarget(): ?string {
    return $this->target;
  }

  /** Getter de la propriété "splitter". */
  public function getSplitter(): ?string {
    return $this->splitter;
  }

  public function isOK(array $criterions=[]): bool {
    return $this->isConfirmed() ?: (
      match ($this->getOperationMode()) {
        self::TARGET => parent::isOK() && !empty($this->target) && !empty($this->splitter),
        default => parent::isOK()
      } ? $this->confirmStatus() : false
    );
  }
}