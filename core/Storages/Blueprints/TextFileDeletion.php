<?php namespace Ultimitaries\Core\Storages\Blueprints;
/**
 * @version 1.0.2
 * @category  Plans de construction
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

/** Chaque objet de cette classe regroupe les arguments nécessaires à la suppression partielle ou complète du contenu d'un fichier texte (voire le fichier lui-même). */
final class TextFileDeletion extends A_TextFile {
  /** Constantes publiques qui permettent de définir le mode de suppression. */
  const ALL_CONTENT = 1, ALL_FILE = 2, TARGET = 3;

  /**
   * Constructeur de la classe courante.
   *
   * @param string  $link Le lien absolu ou relatif d'un fichier texte.
   * @param int $mode  Trois opérations sur les fichiers ciblés sont possibles grâce aux constantes suivantes :
   *  - ALL_CONTENT : supprime l'intégralité du contenu du fichier ;
   *  - ALL_FILE : supprime le fichier ;
   *  - TARGET : supprime des éléments précis dans le fichier grâce au troisième paramètre.
   * @param array|string|null $target [Optionnel] Une chaîne de caractères ou un tableau de chaînes indiquant quelles sont les lignes à supprimer.
   */
  public function __construct(string $link,int $mode,private array|string|null $target=null) {
    parent::__construct($link,$mode);
  }

  /** Getter de la propriété "target". */
  public function getTarget(): array|string|null {
    return $this->target;
  }

  public function isOK(array $criterions=[]): bool {
    return $this->isConfirmed() ?: (parent::isOK() ? $this->confirmStatus() : false);
  }
}