<?php namespace Ultimitaries\Core\Storages\Blueprints;
/**
 * @version 1.0.2
 * @category  Plans de construction
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

/** Chaque objet de cette classe regroupe les arguments nécessaires à la modification partielle ou complète du contenu d'un fichier texte. */
final class TextFileModification extends A_TextFile {
  /** Constantes publiques qui permettent de définir le mode de modification. */
  const APPEND = 1, OVERWRITE = 2, UPDATE = 3;

  /**
   * Constructeur de la classe courante.
   *
   * @param string  $link Le lien absolu ou relatif d'un fichier texte.
   * @param int $mode  Trois opérations sur les fichiers ciblés sont possibles grâce aux constantes suivantes :
   *  - APPEND : ajoute le nouveau contenu à la suite du contenu existant ;
   *  - OVERWRITE : réécrit complètement le fichier avec le nouveau contenu ;
   *  - UPDATE : met à jour les lignes ciblées avec le nouveau contenu.
   * @param array|string|null  $newFileContent  [Optionnel] Le nouveau contenu à ajouter.
   * @param array|string|null  $oldFileContent  [Optionnel] La partie du contenu à remplacer.
   */
  public function __construct(string $link,int $mode,private array|string|null $newFileContent=null,private array|string|null $oldFileContent=null) {
    parent::__construct($link,$mode);

    if($mode === self::UPDATE && empty($oldFileContent))
      throw new \InvalidArgumentException("Content to replace can't be null !");
  }

  /** Getter de la propriété "newFileContent". */
  public function getNewFileContent(): array|string|null {
    return $this->newFileContent;
  }

  /** Getter de la propriété "oldFileContent". */
  public function getOldFileContent(): array|string|null {
    return $this->oldFileContent;
  }

  public function isOK(array $criterions=[]): bool {
    return $this->isConfirmed() ?: (
      parent::isOK() && match($this->getOperationMode()) {
        self::APPEND => !empty($this->newFileContent),
        self::OVERWRITE => true,
        self::UPDATE => !empty($this->newFileContent) && !empty($this->oldFileContent)
      } ? $this->confirmStatus() : false
    );
  }
}