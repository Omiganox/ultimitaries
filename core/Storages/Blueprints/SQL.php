<?php namespace Ultimitaries\Core\Storages\Blueprints;
/**
 * @version 1.2
 * @category  Plans de construction
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

use Ultimitaries\Core\A_Blueprint,
    Ultimitaries\Core\Containers\Arrays\ArrayObjects\A_ArrayObject,
    Ultimitaries\Core\Modules\T_StringMethods,
    Ultimitaries\Core\Modules\T_Utils;

/** Regroupe les informations nécessaires à l'exécution des requêtes SQL, qu'elles soient préparées ou non. */
final class SQL extends A_Blueprint {
  use T_StringMethods, T_Utils;

  /** @var  int $keywordNum Indique le nombre de mot-clés présents dans les requêtes préparées. */
  private int $keywordNum = 0;
  /** @var  ?string  $keywordType  Indique quel type de mot-clé est utilisé dans les requêtes préparées. */
  private ?string $keywordType;

  /**
   * @var string  $query Une requête SQL, préparée ou non.
   * @var array|A_ArrayObject|null  $keywords [Optionnel] Si la requête est préparée, le tableau contiendra les champs nécessaires pour les exécutions.
   */
  public function __construct(private string $query,private array|A_ArrayObject|null $keywords=null) {
    $this->query = \ltrim($query);

    if(!\is_null($keywords))
      $this->keywords = $this->controlKeywords($keywords);
  }

  /** Getter de la propriété "query". */
  public function getQuery(): string {
    return $this->query;
  }

  /**
   * Setter de la propriété "query".
   *
   * @param string  $newQuery La nouvelle requête SQL à stocker.
   */
  public function setQuery(string $newQuery): self {
    $this->query = \ltrim($newQuery);
    return $this->resetStatus();
  }

  /** Getter de la propriété "keywords". */
  public function getKeywords(): array|A_ArrayObject|null {
    return $this->keywords;
  }

  /**
   * Setter de la propriété "keywords".
   *
   * @param array|A_ArrayObject|null  $newKeywords [Optionnel] Les nouvelles données à stocker.
   */
  public function setKeywords(array|A_ArrayObject|null $newKeywords=null): self {
    if(!\is_null($newKeywords)) {
      $this->keywordNum = 0;
      $this->keywords = $this->controlKeywords($newKeywords);
    }
    return $this;
  }

  /** Getter de la propriété "keywordType". */
  public function getKeywordType(): string {
    return $this->keywordType;
  }

  /**
   * Vérifie si le tableau de mots-clés fourni en paramètre est en adéquation avec la requête SQL de l'objet. Cette méthode évite un duplicata du code source dans la classe courante et lève une exception quand une erreur est détectée.
   *
   * @param array|A_ArrayObject $keywords Le tableau de mots-clé à vérifier.
   * @param int $subArrayNum  [Optionnel] Le numéro du sous-tableau analysé.
   *
   * @throws  \InvalidArgumentException
   */
  private function controlKeywords(array|A_ArrayObject $keywords,int $subArrayNum=0): array|A_ArrayObject {
    if(empty($keywords))
      throw new \InvalidArgumentException((empty($subArrayNum) ? "Keywords array" : "Sub-array n°$subArrayNum")." is empty !");
    if(empty($subArrayNum))
      $this->getKeywordsInfo();
    $isList = $keywords instanceof A_ArrayObject ? $keywords->isList() : $this->array_is_list($keywords);
    
    switch($this->keywordType) {
      case "?":
        if(!$isList)
          throw new \InvalidArgumentException("List of keywords expected, associative array found instead !");
        if(\is_array($keywords[0])) {
          foreach($keywords as $key => $keyword)
            $this->controlKeywords($keyword,$key+1);
        }
        else
          if(!$this->matchKeywords($keywords))
            $this->throwThis($subArrayNum,$keywords);
        break;
      case ":":
        if($isList)
          foreach($keywords as $key => $array)
            $this->controlKeywords($array,$key+1);
        else {
          if(!$this->matchKeywords($keywords))
            $this->throwThis($subArrayNum,$keywords);
          if(!$this->containsWords($this->query,\array_keys($keywords),$missingKeys))
            throw new \InvalidArgumentException("Sub-array n°$subArrayNum contains unexpected keys '".\join("', '",$missingKeys)."' !");
        }
    }
    return $keywords;
  }

  /** Définit les propriétés "keywordType" et "KeywordNum" en fonction du contenu de "query". Le nombre de mot-clés détectés est retourné. */
  private function getKeywordsInfo(): int {
    foreach([":","?"] as $keyword)
      if(\str_contains($this->query,$keyword)) {
        $this->keywordType = $keyword;
        return $this->keywordNum = \substr_count($this->query,$keyword);
      }
    $this->keywordType = null;
    return $this->keywordNum = 0;
  }

  /** Trouve et extrait le nom de la table de la requête SQL. */
  public function getTableName(): string {
    \preg_match("/(from|into|table|^update)\s+\w+/i",$this->query,$result);
    return \ltrim(\explode(" ",$result[0])[1]);
  }

  /** Vérifie si des mot-clés ont été renseignés ou non. */
  public function hasKeywords(): bool {
    return $this->keywordNum > 0;
  }

  /** 
   * @param array $criterions Un tableau sous la forme "['start' => $value, 'method' => $value]". Si les valeurs de "start" et "method" sont identiques, "method" n'est pas obligatoire.
   * 
   * @throws \InvalidArgumentException
   */
  public function isOK(array $criterions=[]): bool {
    if(!empty($criterions)) {
      if(!\str_starts_with($this->query,$criterions["start"]))
        throw new \InvalidArgumentException("'".$criterions["start"]."' not expected into '".$criterions[isset($criterions["method"]) ? "method" : "start"]."' method !");
    }
    return $this->isConfirmed() ?: (
      match($this->getKeywordsInfo()) {
        0 => !empty($this->query),
        default => !empty($this->query) && !empty($this->keywords)
      } ? $this->confirmStatus() : false
    );
  }

  /**
   * Vérifie si la requête SQL possède autant de mots-clés que le tableau renseigné en paramètre.
   *
   * @param array|A_ArrayObject $keywords Le tableau de mots-clés à vérifier.
   */
  private function matchKeywords(array|A_ArrayObject $keywords): bool {
    return $this->keywordNum === \count($keywords);
  }

  /** Juste pour éviter un nouveau duplicata ET éviter d'initialiser l'exception à chaque nouvelle récursivité. */
  private function throwThis(int $subNum,array|A_ArrayObject $keywords): void {
    throw new \InvalidArgumentException("Sub-array n°$subNum doesn't have same numbers of ".($this->areEquals($this->keywordType,":") ? "keywords" : "values")." expected (".\count($keywords)." / ".$this->keywordNum.") !");
  }
}