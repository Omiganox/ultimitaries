<?php namespace Ultimitaries\Core\Storages\Blueprints;
/**
 * @version 0.4
 * @category  Plans de construction
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

use Ultimitaries\Core\A_Blueprint;

/** Regroupe les informations générales des plans de construction qui gèrent les opérations liées aux fichiers texte. */
abstract class A_TextFile extends A_Blueprint {
  /**
   * Constructeur de la classe courante.
   *
   * @var string  textFileLink Le lien absolu ou relatif d'un fichier.
   * @var int $operationMode  Entier correspondant à l'une des énumérations des classes filles.
   * 
   * @throws  \InvalidArgumentException
   */
  public function __construct(private string $textFileLink,private int $operationMode) {
    $className = \get_class($this);

    if(!$this::matchConstant($className,$operationMode))
      throw new \InvalidArgumentException("Specified mode doens't match any '$className'\'s constants !");
  }

  /** Getter de la propriété "operationMode". */
  public function getOperationMode(): int {
    return $this->operationMode;
  }

  /** Getter de la propriété "textFileLink". */
  public function getTextFileLink(): string {
    return $this->textFileLink;
  }

  public function isOK(array $criterions=[]): bool {
    return !empty($this->textFileLink) && !\is_null($this->operationMode);
  }
}