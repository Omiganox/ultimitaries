<?php namespace Ultimitaries\Core\Storages;
/**
 * @version 0.4
 * @category  Stockage externe des données
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

use Ultimitaries\Core\A_Blueprint,
    Ultimitaries\Core\Security\I_Cipher,
    Ultimitaries\Core\Storages\Blueprints\SQL,
    Ultimitaries\Core\Storages\Blueprints\TextFileDeletion as TFD,
    Ultimitaries\Core\Storages\Blueprints\TextFileGetter as TFG,
    Ultimitaries\Core\Storages\Blueprints\TextFileModification as TFM;

/** Les objets de cette classe encapsulent les autres objets "A_Storage" et dé-cryptent les données qu'ils exploitent (DP "Décorateur"). */
class EncryptedStorage extends A_Storage {
  /**
   * @var DatabaseStorage|TextFileStorage $storage  L'objet en charge du stockage des données.
   * @var I_Cipher  $cipher L'objet permettant de dé-crypter des données.
   */
  public function __construct(private DatabaseStorage|TextFileStorage $storage,private ?I_Cipher $cipher=null) {}

  /** @param  SQL|TFM $blueprint */
  public function add(A_Blueprint $blueprint): self {
    if($this->checkThis($blueprint,"add")) {
      $isSQL = $this->storage instanceof DatabaseStorage;

      if($isSQL) {
        if($blueprint->hasKeywords())
          $this->createLog(new \Exception("Object '".__CLASS__."' can't cipher datas of unprepared queries..."));
        else
          $blueprint->setKeywords($this->cipherKeywords($blueprint->getKeywords()));
      }
      else
        $cryptedBlueprint = new TFM(
          $blueprint->getTextFileLink(),
          $blueprint->getOperationMode(),
          $this->cipher->encode($blueprint->getNewFileContent())
        );
      $this->storage->add($isSQL ? $blueprint : $cryptedBlueprint);
    }
    return $this;
  }

  /**
   * Vérifie si le plan de construction fourni à une méthode lui convient en fonction du type de la propriété "storage".
   *
   * @param A_Blueprint $blueprint  Le plan de construction à vérifier.
   * @param string  $methodName La méthode associée à la vérification.
   */
  protected function checkThis(A_Blueprint $blueprint,string $methodName): bool {
    $className = \get_class($blueprint);
    $expected = match($className) {
      "SQL" => true,
      "TextFileDeletion" => $this->areEquals($methodName,"delete"),
      "TextFileGetter" => $this->areEquals($methodName,"get"),
      "TextFileModification" => \in_array($methodName,["add","create","update"]),
      default => false
    };
    if(!$expected)
      $this->createLog(new \InvalidArgumentException("'$className' object received in '".\get_class($this->storage)."::$methodName' !"));
    return $expected;
  }

  /**
   * Crypte les informations d'une requête SQL préparée.
   * 
   * @param array $keywords Les informations à protéger.
   */
  private function cipherKeywords(array $keywords): array {
    foreach($keywords as $keyword) {

    }
    return [];
  }

  /** @param  SQL|TFM $blueprint */
  public function create(A_Blueprint $blueprint): self {

    return $this;
  }

  /** @param  SQL|TFD $blueprint */
  public function delete(A_Blueprint $blueprint): self {

    return $this;
  }

  public function exist(string $targetedContainer,bool $exceptionRaise=true): bool {
    return $this->storage->exist($targetedContainer,$exceptionRaise);
  }

  /** @param  SQL|TFG $blueprint */
  public function get(A_Blueprint $blueprint): mixed {

  }

  /** @param  SQL|TFM $blueprint */
  public function update(A_Blueprint $blueprint): self {

    return $this;
  }
}