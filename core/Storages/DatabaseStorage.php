<?php namespace Ultimitaries\Core\Storages;
/**
 * @version 1.2
 * @category  Stockage externe des données
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

use Ultimitaries\Core\Containers\Arrays\ArrayObjects\ArrayList,
    Ultimitaries\Core\Parsers\Database\DatabaseParser,
    Ultimitaries\Core\Storages\Blueprints\SQL,
    Ultimitaries\Core\Storages\Blueprints\TextFileGetter as TFG;

/** Les objets de cette classe sauvegardent, chargent ou modifient les données d'un composant dans les tables d'une base de données. */
final class DatabaseStorage extends A_Storage {

  /** @var  DatabaseParser  $dbParser L'objet "DatabaseParser" en charge des opérations sur la base de données. */
  private DatabaseParser $dbParser;
  /** @var  ArrayList $tables Contient le nom des tables dont l'existence a été confirmé dans la base de données. */
  private ArrayList $tables;

  /** @param  TFG $specs  Le plan de construction contenant les informations nécessaires à la création de l'objet. */
  public function __construct(TFG $specs) {
    parent::__construct();
    $this->dbParser = DatabaseParser::getInstanceFor($specs);
    $this->tables = new ArrayList;
  }

  /** Retourne une version simplifiée de l'objet depuis la fonction native "var_dump()". */
  public function __debugInfo(): array {
    return ["checkedTables" => \count($this->tables)];
  }

  /** Getter de la propriété "dbParser". */
  public function getDbParser(): DatabaseParser {
    return $this->dbParser;
  }

  /**
   * Insère des données dans la table d'une base de données.
   *
   * @param SQL $blueprint  Plan de construction contenant les informations nécessaires aux opérations de la méthode.
   */
  public function add(SQL $blueprint): self {
    if($this->checkThis($blueprint,["start" => "insert","method" => "add"]) & $this->exist($blueprint->getTableName()))
      $this->dbParser->execute($blueprint);
    return $this;
  }

  /**
   * Vérifie si la requête SQL correspond aux attentes des méthodes de la classe courante.
   *
   * @param SQL $blueprint  Le plan de construction à vérifier.
   * @param array $criterions Des critères de validation pour le premier argument.
   */
  protected function checkThis(SQL $blueprint,array $criterions): bool {
    try {
      return $blueprint->isOK($criterions);
    }
    catch(\Exception $e) {
      $this->createLog($e);
      return false;
    }
  }

  /**
   * Crée une nouvelle structure dans une base de données.
   *
   * @param SQL $blueprint  Plan de construction contenant les informations nécessaires aux opérations de la méthode.
   */
  protected function create(SQL $blueprint): self {
    if($this->checkThis($blueprint,["start" => "create"]) & !$this->exist($blueprint->getQuery(),false))
      $this->dbParser->execute($blueprint);
    return $this;
  }

  /**
   * Supprime des données dans la table d'une base de données.
   *
   * @param SQL $blueprint  Plan de construction contenant les informations nécessaires aux opérations de la méthode.
   */
  public function delete(SQL $blueprint): self {
    $tableName = $blueprint->getTableName();

    if($this->checkThis($blueprint,["start" => "delete"]) & $this->exist($tableName)) {
      $this->dbParser->execute($blueprint);
      $this->dbParser->readjustAutoIncrement($tableName);
    }
    return $this;
  }

  public function exist(string $tableName,bool $exceptionRaise=true): bool {
    if($this->tables->hasValue($tableName) ?: $this->dbParser->checkTableExistence($tableName)) {
      if(!$this->tables->hasValue($tableName))
        $this->tables->push($tableName);
      return true;
    }
    return $exceptionRaise ? parent::exist("Table '$tableName'") : false;
  }

  /**
   * Récupère des informations dans la table d'une de base de données.
   *
   * @param SQL $blueprint  Plan de construction contenant les informations nécessaires aux opérations de la méthode.
   */
  public function get(SQL $blueprint): mixed {
    return $this->checkThis($blueprint,["start" => "select","method" => "get"]) & $this->exist($blueprint->getTableName()) ? $this->dbParser->execute($blueprint) : null;
  }

  /**
   * Met à jour des données dans la table d'une base de données.
   *
   * @param SQL $blueprint  Plan de construction contenant les informations nécessaires aux opérations de la méthode.
   */
  public function update(SQL $blueprint): self {
    if($this->checkThis($blueprint,["start" => "update"]) & $this->exist($blueprint->getTableName()))
      $this->dbParser->execute($blueprint);
    return $this;
  }
}