<?php namespace Ultimitaries\Core\Parsers\Database\Exceptions;
/**
 * @version 0.2
 * @category  Exceptions personnalisées
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

/** Levée dans le cas où la méthode "getPrimaryOf(1)" des interpréteurs de base de données récupère plus d'une clé primaire dans une table de données. Comme aucune solution viable n'a pas encore été trouvé pour différencier les clés primaires d'une table intermédiaire, on évite de mener à terme cette opération, pour le moment. */
final class MultiplePrimaryKeysException extends \Exception {}