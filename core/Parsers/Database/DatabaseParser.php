<?php namespace Ultimitaries\Core\Parsers\Database;
/**
 * @version 1.0
 * @category  Interpréteur de base de données
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

use Ultimitaries\Core\Containers\Arrays\ArrayObjects\A_ArrayObject,
    Ultimitaries\Core\Containers\Arrays\ArrayObjects\Map,
    Ultimitaries\Core\Modules\T_Utils,
    Ultimitaries\Core\Modules\Logs\T_Logs,
    Ultimitaries\Core\Parsers\Database\Exceptions\MultiplePrimaryKeysException,
    Ultimitaries\Core\Storages\TextFileStorage,
    Ultimitaries\Core\Storages\Blueprints\SQL,
    Ultimitaries\Core\Storages\Blueprints\TextFileGetter as TFG;

/** Chaque objet de cette classe gère sa connexion à une base de données et les requêtes SQL qui y sont effectuées. Il ne peut y avoir plus d'une instance pour la même base de données. */
class DatabaseParser {
  use T_Utils, T_Logs;

  /** @var self  $activeSessions  Contient les informations des bases de données qui sont actuellement connectés par un objet PDO. */
  private static Map $activeSessions;
  /** @var \PDO $pdo  L'objet "PDO" connecté à une base de données. */
  private ?\PDO $pdo;

  /** @var string  $sectionName  Le nom de la section du fichier de configuration où ont été récupérés les informations de connexion à une base de données. */
  private function __construct(private string $sessionName) {
    $this->integrateLogWriter();
    $database = self::$activeSessions[$sessionName];
    try {
      $this->pdo = new \PDO(
        "mysql:host=".$database["HOST"].";dbname=".$database["NAME"].";charset=UTF8",
        $database["USER"],
        $database["PASS"],
        [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,\PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC]
      );
    }
    catch(\PDOException $e) {
      $this->createLog($e);
      $this->pdo = null;
    }
  }

  private function __clone() {}

  public function __debugInfo(): array {
    return ["pdo" => \is_null($this->pdo) ? "null" : "connected","logWriter" => \is_null($this->logWriter) ? "none" : "initialized"];
  }

  /** Quand une instance est détruite, on efface les informations liées à la base de données à laquelle son objet PDO était connecté. */
  public function __destruct() {
    unset(self::$activeSessions[$this->sessionName]);
  }

  /**
   * Vérifie si la table renseignée existe ou non dans la base de données.
   *
   * @param  string  $tableName  Le nom de la table à vérifier.
   */
  public function checkTableExistence(string $tableName): ?bool {
    if($this->isConnected()) {
      $tables = $this->pdo->query("show tables");

      while($result = $tables->fetch())
        if(\in_array($tableName,$result))
          return true;
      return false;
    }
    return null;
  }

  /**
   * Exécute une requête SQL - préparée ou non - en toute sécurité.
   *
   * @param SQL  $blueprint L'objet "SQL" contenant la requête à exécuter.
   */
  public function execute(SQL $blueprint): ?array {
    if($this->isConnected()) {
      if(\is_null($blueprint->getKeywords()))
        $queryResult = $this->pdo->query($blueprint->getQuery())->fetchAll();
      else {
        $preparedQuery = $this->pdo->prepare($blueprint->getQuery());
        $keywords = $blueprint->getKeywords();
        $isList = match($blueprint->getKeywordType()) {
          ":" => $keywords instanceof A_ArrayObject ? $keywords->isList() : $this->array_is_list($keywords),
          "?" => \is_array($keywords[0])
        };
        if($isList) {
          $queryResults = [];

          foreach($keywords as $keyword) {
            $preparedQuery->execute($keyword);
            $queryResults[] = $preparedQuery->fetchAll();
          }
          return $queryResults; // Tableau tri-dimensionnel
        }
        else {
          $preparedQuery->execute($keywords);
          $queryResult = $preparedQuery->fetchAll();
        }
      }
      return \count($queryResult) === 1 ? $queryResult[0] : $queryResult;
    }
    return null;
  }

  /**
   * Génère un nouvel identifiant dans une table. L'option "AUTO_INCREMENT" des tables SQL possède un défaut gênant : il ne comble pas les "trous" générés par les requêtes de suppression et continue d'incrémenter une valeur par défaut qu'il attribue aux nouveaux identifiants. Cette méthode permet de l'éviter en vérifiant au préalable le contenu de la table concernée par une mise à jour : si un trou est détectée, il sera comblé avec le bon identifiant numérique.
   *
   * @param  string  $tableName  Le nom de la table à vérifier.
   */
  public function generateID(string $tableName): int|string {
    if($this->isConnected()) {
      $primaryKey = $this->getPrimaryOf($tableName);
      $query = new SQL("select count(*) as total, max($primaryKey) as max from $tableName");
      $queryResult = $this->execute($query);

      if($queryResult["total"] !== 0 && $queryResult["max"] > $queryResult["total"]) {
        $queryResult = $this->execute($query->setQuery("select $primaryKey from $tableName order by $primaryKey asc"));
        $newID = 1;

        foreach($queryResult as $array)  {
          if($array[$primaryKey] != $newID)
            return $newID;
          ++$newID;
        }
      }
    }
    return "default";
  }

  /**
   * Crée et retourne une instance connectée à une base de données spécifique. Cette méthode empêche la création de multiples instances connectées à la même base de données (implémentation customisée du DP "Singleton").
   *
   * @param TFG $specs  Le plan de construction contenant les informations nécessaires à la création de l'objet.
   * 
   * @throws  \ReflectionException
   */
  public static function getInstanceFor(TFG $specs): self {
    $pdoArray = TextFileStorage::getInstance()->get($specs)->getArrayCopy();
    $fileSection = $specs->getTarget();
    
    if(empty(self::$activeSessions))
      self::$activeSessions = new Map;
    else
      foreach(self::$activeSessions as $session)
        if(($pdoArray <=> $session) == 0)
          throw new \ReflectionException("An active session already exists for this database's connection : $fileSection");
    self::$activeSessions[$fileSection] = $pdoArray;
    return new self($fileSection);
  }

  /**
   * Récupère la clé primaire d'une table de la base de donnée à laquelle l'instance appelante est connectée. La méthode ne gère pas la récupération des clés primaires d'une table intermédiaire ; auquel cas, une exception sera levée.
   *
   * @param string  $tableName  Le nom de la table ciblée.
   */
  public function getPrimaryOf(string $tableName): ?string {
    if($this->isConnected()) {
      $result = $this->pdo?->query("DESCRIBE $tableName");
      $primaryNum = 0;

      while($columnDatas = $result->fetch()) {
        if($this->areEquals($columnDatas["Key"],"PRI")) {
          $primary = $columnDatas["Field"];
          ++$primaryNum;
        }
        if($primaryNum > 1)
          throw new MultiplePrimaryKeysException("Intermediate table detected, operation aborted !");
      }
    }
    return $primary ?? null;
  }

  /** Vérifie si l'instance appelante est connectée à la base de données. */
  public function isConnected(): bool {
    return !\is_null($this->pdo);
  }

  /**
   * Réajuste la valeur de la propriété "AUTO_INCREMENT" d'une table donnée.
   *
   * @param  string  $tableName  Le nom de la table à inspecter.
   */
  public function readjustAutoIncrement(string $tableName): void {
    if($this->isConnected()) {
      $autoInc = $this->execute(new SQL("show table status like '$tableName'"))["Auto_increment"];
      $maxID = $this->execute(new SQL("select max(".$this->getPrimaryOf($tableName).") as max from $tableName"))["max"]+1;

      if($autoInc !== $maxID)
        $this->execute(new SQL("alter table $tableName AUTO_INCREMENT = $maxID"));
    }
  }
}