<?php namespace Ultimitaries\Core\Parsers\TextFormats;
/**
 * @version 0.2
 * @category  Interpréteurs de format de fichier
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

/** Chaque objet de cette classe encode et décode les informations d'après le format "XML". */
class XML extends A_TextFormat {
  /**
   * Convertit un contenu XML en un tableau exploitable.
   * 
   * @param string  $fileContent Le lien ou le contenu du fichier XML à décoder.
   * @param array $xmlKeys  Les noms des balises XML à décoder.
   */
  public function decode(string $fileLink,array $xmlKeys): ?array {
    return [];
  }

  public function encode(array $values): string {
    return "";
  }
}