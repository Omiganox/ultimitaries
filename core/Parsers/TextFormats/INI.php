<?php namespace Ultimitaries\Core\Parsers\TextFormats;
/**
 * @version 1.0
 * @category  Interpréteurs de format de fichier
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

/** L'instance de cette classe encode et décode les informations d'après le format PHP (".ini"). */
final class INI extends A_TextFormat {
  /**
   * Convertit un contenu INI en un tableau exploitable.
   * 
   * @param string  $fileContent Le lien ou le contenu du fichier INI à décoder.
   */
  public function decode(string $fileContent): ?array {
    $iniContent = \is_file($fileContent) ? \parse_ini_file($fileContent,true) : \parse_ini_string($fileContent,true);

    if(\is_bool($iniContent))
      return $this->sendLog("INI");
    return $iniContent;
  }

  public function encode(array $values): string {
    $fileContent = "";
    $string = function(mixed $data) {
      return \is_string($data) ? "\"$data\"" : $data;
    };

    foreach($values as $key => $value) {
      if(\is_int($key) && !\is_array($value)) { // Commentaire
        $value = \is_string($value) ? \ltrim($value) : (string) $value;

        if(!\str_starts_with($value,";"))
          $newLine = \preg_replace("/(^\/\/|#+)\s*|^/m","; ",$value); // Regex : "(//|#) et espacement|(début de ligne)"
      }
      else {
        if(\is_array($value)) {
          $newLine = "";

          if($this->array_is_list($value))
            foreach($value as $data)
              $newLine .= $key."[] = ".$string($data)."\n";
          else {
            $newLine .= "[$key]\n\n";

            foreach($value as $subKey => $data)
              $newLine .= "$subKey = ".$string($data)."\n";
          }
        }
        else
          $newLine = "$key = ".$string($value);
      }
      $fileContent .= $newLine."\n";
    }
    return $fileContent;
  }
}
