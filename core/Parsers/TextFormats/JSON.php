<?php namespace Ultimitaries\Core\Parsers\TextFormats;
/**
 * @version 1.0
 * @category  Interpréteurs de format de fichier
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

/** L'instance de de cette classe encode et décode les informations d'après le format "JSON". */
class JSON extends A_TextFormat {
  /**
   * Convertit un contenu JSON en un tableau exploitable.
   * 
   * @param string  $fileContent Le lien ou le contenu du fichier JSON à décoder.
   */
  public function decode(string $content): ?array {
    $jsonContent = \json_decode($content,true);

    if(\is_null($jsonContent))
      return $this->sendLog("JSON");
    return $jsonContent;
  }

  public function encode(array $values): string {
    return \json_encode($values);
  }
}
