<?php namespace Ultimitaries\Core\Parsers\TextFormats;
/**
 * @version 0.2
 * @category  Interpréteurs de format de fichier
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

use Ultimitaries\Core\Modules\T_Abstraction,
    Ultimitaries\Core\Modules\T_SimpleSingleton,
    Ultimitaries\Core\Modules\Logs\T_Logs;

/** Fournit les prototypes des méthodes nécessaires à l'implémentation des interpréteurs de format de fichier. */
abstract class A_TextFormat {
  use T_Abstraction, T_SimpleSingleton, T_Logs;

  protected function __construct() {
    $this->integrateLogWriter();
    $this->implementsMethods("decode");
  }

  /**
   * Génère un log précis en cas d'erreur.
   *
   * @param string  $content  L'extension du fichier posant problème.
   */
  protected function sendLog(string $extension) {
    $this->createLog(new \ParseError("Text received in argument doesn't meet $extension standards !"));
    return null;
  }

  /**
   * Convertit un tableau de données en un texte au format prédéfini.
   *
   * @param array $values Le tableau de données à encoder.
   */
  abstract public function encode(array $values): string;
}
