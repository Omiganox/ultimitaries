<?php namespace Ultimitaries\Core\Parsers\TextFormats\Factories;
/**
 * @version 0.1
 * @category  Fabriques abstraites
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

use Ultimitaries\Core\Parsers\TextFormats\A_TextFormat,
    Ultimitaries\Core\Parsers\TextFormats\INI,
    Ultimitaries\Core\Parsers\TextFormats\JSON,
    Ultimitaries\Core\Parsers\TextFormats\XML;

/** Crée des objets "A_TextFormatParser" en fonction de l'extension des fichiers. */
final class TextFormatParser {
  /** Contantes de classe indiquant quel type d'interpréteur va être retourné. */
  const INI = "ini", JSON = "json", XML = "xml";

  private function __construct() {}
  private function __clone() {}

  /**
   * Retourne une instance de la classe attendue à partir du paramètre reçu ; ou null s'il ne correspond à rien de valide.
   *
   * @param string  $fileExtension  L'extension d'un fichier texte.
   * @param ?object  $parser [Optionnel] Un interpréteur de fichier existant.
   */
  public static function getInstanceFor(string $fileExtension,?object $parser=null): ?A_TextFormat {
    if(\str_contains($fileExtension,"."))
      $fileExtension = \pathinfo($fileExtension,PATHINFO_EXTENSION);
    return match(\strtolower($fileExtension)) {
      self::INI => INI::getInstance(),
      self::JSON => JSON::getInstance(),
      //self::XML => XML::getInstance(),
      default => null
    };
  }
}