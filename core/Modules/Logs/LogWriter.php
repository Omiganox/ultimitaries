<?php namespace Ultimitaries\Core\Modules\Logs;
/**
 * @version 1.1
 * @category  Collecteur de logs
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

use Ultimitaries\Core\Modules\T_SimpleSingleton,
    Ultimitaries\Core\Parsers\TextFormats\Factories\TextFormatParser,
    Ultimitaries\Core\Storages\Blueprints\TextFileModification as TFM,
    Ultimitaries\Core\Storages\Factories\Storage as F_S;

/** L'instance unique de cette classe collecte les messages d'erreur dans les composants qui en génèrent, puis les publie dans des fichiers texte prévus à cet effet. */
final class LogWriter {
  use T_SimpleSingleton;

  /** Constantes de classe renseignant le format des dates, la localisation des fichiers de logs dans l'arborescence du framework, ainsi que leur taille maximale, leur préfixe par défaut et leur nombre autorisé. */
  private const DATE_FORMAT = "d/m/y - H:i:s", FILE = ["LINK" => __DIR__."/../../../debug/","MAX_NUMBER" => 4,"MAX_SIZE" => 1073741824,"PREFIX" => "logs_"];

  /** @var array  $logs  Le tableau qui va contenir tous les messages d'erreur collectés. */
  private array $logs = [];

  /** Retourne une version simplifiée de l'objet depuis la fonction native "var_dump()". */
  public function __debugInfo(): array {
    return ["numberOfLogs" => \count($this->logs)];
  }

  /** Vide le contenu du tableau de logs de l'objet dans un fichier texte lorsque l'instance s'apprête à être détruite. */
  public function __destruct() {
    if(!empty($this->logs))
      \file_put_contents(
        self::FILE["LINK"].self::FILE["PREFIX"].$this->selectFileToWrite($overwrite).".txt",
        (new \DateTime)->format(self::DATE_FORMAT)."\n\n".\join("\n",$this->logs)."\n",
        $overwrite ? LOCK_EX : FILE_APPEND|LOCK_EX
      );
      /*
      (F_S::getInstanceOf(F_S::TF_S))->create(new TFM(
        self::FILE["LINK"].self::FILE["PREFIX"].$this->selectFileToWrite($overwrite).".json",
        TFM::UPDATE,
        (TextFormatParser::getInstanceFor("JSON"))->encode([(new \DateTime)->format(self::DATE_FORMAT) => $this->logs])."\n}",
        "}\n}"
      )); //*/
  }

  /**
   * Ajoute un nouveau log en récupérant les informations liées à un objet et à l'exception qu'il a levé.
   *
   * @param string  $objectName  Le nom de l'objet.
   * @param \Error|\Exception $e  L'exception levée.
   */
  final public function add(string $objectName,\Error|\Exception $e): void {
    $this->logs[] = "[$objectName] {$e->getMessage()}\n{$e->getTraceAsString()}\n";
  }

  /**
   * Retourne le numéro du fichier de logs dans lequel doit être inséré les nouveaux logs.
   *
   * @param ?bool  $overwrite  Selon le résultat du script, autorise ou non la réécriture du fichier.
   */
  private function selectFileToWrite(?bool &$overwrite): int { // Supprimer l'argument avec la nouvelle version de "__destruct()" ?
    $logFiles = [];

    foreach(\array_slice(\scandir(self::FILE["LINK"]),2) as $fileName) {
      if(\str_starts_with($fileName,self::FILE["PREFIX"])) {
        $fileNum = \substr($fileName,\strlen(self::FILE["PREFIX"]),1);
        $fileName = self::FILE["LINK"].$fileName;
        $logFiles[$fileNum] = array("mtime" => \filemtime($fileName),"size" => \filesize($fileName));
      }
    }
    if(empty($logFiles))
      return 1;
    else {
      $oldestNum = 1;

      foreach($logFiles as $num => $file) {
        if($file["size"] < self::FILE["MAX_SIZE"]) { // Inférieur à 1 Go
          $overwrite = false;
          return $num;
        }
        else { // On cherche le fichier le plus ancien au cas où
          if($file["mtime"] < $logFiles[$oldestNum]["mtime"])
            $oldestNum = $num;
        }
      }
      $overwrite = true;
      $fileCount = \count($logFiles);
      return $fileCount < self::FILE["MAX_NUMBER"] ? $fileCount + 1 : $oldestNum;
    }
  }
}