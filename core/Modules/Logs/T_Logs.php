<?php namespace Ultimitaries\Core\Modules\Logs;
/**
 * @version 0.3
 * @category  Collecteur de logs
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

/** Ce trait fournit le nécessaire pour intégrer et utiliser le collecteur de logs. */
trait T_Logs {
  /** @var  LogWriter  $logWriter L'instance unique de la classe éponyme. */
  private LogWriter $logWriter;

  /**
   * Génère un nouveau log en récupérant les informations liées à l'objet concerné et à l'exception levée.
   *
   * @param \Error|\Exception $e  L'exception levée.
   */
  protected function createLog(\Error|\Exception $e): bool {
    $this->logWriter->add(\get_class($this),$e);
    return false;
  }

  /** Permet d'affecter l'objet unique "LogWriter" à l'instance d'une autre classe. */
  protected function integrateLogWriter(): void {
    $this->logWriter = LogWriter::getInstance();
  }
}