<?php namespace Ultimitaries\Core\Modules;
/**
 * @version 0.5
 * @category  Modules généraux
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

/** Fournit des méthodes qui opèrent sur les chaînes de caractères. */
trait T_StringMethods {
  /**
   * Vérifie si un ou plusieurs termes donnés appartiennent à une chaîne de caractères. Contrairement à la fonction "str_contains", cette méthode ne reconnaît que les termes entiers.
   *
   * @param string  $string La chaîne de caractères à analyser.
   * @param array $searchedWords Les termes à trouver.
   * @param array &$missings  [Optionnel] Si renseignée, la variable contiendra les mots manquants.
   */
  protected function containsWords(string $string,array $searchedWords,?array &$missings=[]): bool {
    foreach($searchedWords as $word) {
      if(!(\preg_match("/(^|[\s\W])$word($|[\s\W])/",$string) === 1))
        $missings[] = $word;
    }
    return empty($missings);
  }

  /**
   * Retourne le premier mot d'une chaîne de caractères (ou null si la chaîne est vide).
   *
   * @param string  $string La chaîne de caractères à analyser.
   */
  protected function getFirstWord(string $string): ?string {
    if(($string <=> "") === 0)
      return null;
    else {
      \preg_match("/(^|\s+)\w+/i",$string,$result);
      return \trim($result[0]);
    }
  }

  /**
   * Modifie une chaîne de caractères en remplaçant tous les doublons consécutifs d'un terme précis par un seul exemplaire de celui-ci.
   *
   * @param string  $string La chaîne de caractères à traiter.
   * @param string  $duplicate  Le terme servant à identifier les doublons consécutifs.
   */
  protected function mergeSubsequentDuplicates(string $string,string $duplicate): string {
    while(\substr_count($string,$duplicate.$duplicate) > 0)
      $string = \str_replace($duplicate.$duplicate,$duplicate,$string);
    return $string;
  }

  /**
   * Retire tous les caractères indésirables d'une chaîne de caractères.
   *
   * @param string  $string La chaîne de caractères à traiter.
   * @param string  $charset  [Optionnel] Le format unicode du système, afin d'adapter le filtrage des caractères.
   */
  protected function slugify(string $string,string $charset="UTF-8"): string {
    $string = \htmlentities($string,ENT_NOQUOTES,$charset);
    $string = \preg_replace("/&([A-Za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);/",'\1',$string);
    $string = \preg_replace("/&([A-Za-z]{2})(?:lig);/",'\1',$string); // Pour les ligatures e.g. '&oelig;"
    $string = \preg_replace("/\^|\s|'|\"|\(|\)|\[|\]|&([A-Za-z]{3});/",'_',$string);
    return \preg_replace("/\\\|\//",'-',$string);
  }
}