<?php namespace Ultimitaries\Core\Modules;
/**
 * @version 0.4
 * @category  Modules généraux
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

/** Fournit des méthodes générales. */
trait T_Utils {
  /**
   * Vérifie si deux variables sont identiques. La comparaison stricte est possible pour :
   *  - les valeurs numériques ;
   *  - les tableaux ;
   *  - et les objets (les références seron testées en plus du type et des propriétés).
   *
   * @param mixed  $a La première variable quelconque.
   * @param mixed  $b [Optionnel] La seconde variable à comparer avec la première.
   * @param bool $strict [Optionnel] Booléen indiquant si la comparaison doit être stricte (type/index compris ou non).
   */
  protected function areEquals(mixed $a,mixed $b=null,bool $strict=false): bool {
    return \is_array($a) && \is_array($b) ?
      ($strict ? ($a <=> $b) == 0 : empty(\array_diff($a,$b))) :
      ($strict ? $a === ($b ?? $this) : ($a <=> $b) == 0);
  }

  protected function array_is_list(array $array): bool { // Temporaire (<= PHP 8.1)
    return empty($array) || \array_keys($array) === \range(0,\count($array)-1);
  }
}