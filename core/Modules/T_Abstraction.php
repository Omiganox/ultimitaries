<?php namespace Ultimitaries\Core\Modules;
/**
 * @version 0.1
 * @category  Modules généraux
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

use Ultimitaries\Core\Containers\Arrays\ArrayObjects\ArrayList;

/** Fournit des méthodes qui vérifient la cohérence des structures logiciel d'un projet. */
trait T_Abstraction {
  use T_Utils;

  /** @var  ArrayList $checkedClasses Les classes déjà vérifiées. */
  private static ArrayList $checkedClasses;

  /**
   * Vérifie si une classe contient les méthodes renseignées en paramètre. Comme la surharge d'une définition de méthode est impossible en PHP, cette méthode permet de contourner le problème en s'assurant qu'une classe fille contient des méthodes qui sont normalement implémentées dans la classe mère.
   * 
   * @param string[]  $methods  Les noms des méthodes à contrôler.
   * 
   * @throws  \ReflectionException
   */
  protected function implementsMethods(string ...$methods): void {
    $class = \get_class($this);

    if(!isset($this::$checkedClasses))
      $this::$checkedClasses = new ArrayList;
    if(!$this::$checkedClasses->hasValue($class)) {
      $missings = \array_diff($methods,\array_intersect($methods,\get_class_methods($this)));

      if(!empty($missings))
        throw new \ReflectionException("Class '$class' doesn't implements the following methods : '".\join("', '",$missings)."' !");
    }
  }

  /**
   * Vérifie si le second paramètre est identique à l'une des constantes de la classe renseignée.
   *
   * @param string  $class  Le nom de la classe supposée contenir des constantes.
   * @param int|string  $data Une variable supposée correspondre à l'une des constantes du premier paramètre.
   */
  protected static function matchConstant(string $class,int|string $data): bool {
    return \in_array($data,(new \ReflectionClass($class))->getConstants());
  }
}