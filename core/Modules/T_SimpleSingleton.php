<?php namespace Ultimitaries\Core\Modules;
/**
 * @version 0.1
 * @category  Modules généraux
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

/** Fournit l'implémentation basique pour les classes qui doivent retourner une instance unique. */
trait T_SimpleSingleton {
  /** @var  ?object  L'instance unique de la classe. */
  private static ?object $instance = null;

  protected function __construct() {
    if(!\is_bool(\get_parent_class($this)))
      parent::__construct();
  }

  private function __clone() {}

  /** Retourne l'instance unique de la classe appelée. */
  public static function getInstance(): static {
    if(\is_null(static::$instance))
      static::$instance = new static();
    return static::$instance;
  }
}