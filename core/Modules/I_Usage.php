<?php namespace Ultimitaries\Core\Modules;
/**
 * @version 0.2
 * @category  Modules généraux
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

/** La méthode de cette interface détermine si un composant peut être utilisé par d'autres dans le projet ou non. */
interface I_Usage {
  /** Retourne vrai si le composant peut être utilisé, faux sinon. */
  public function canBeUsed(): bool;
}