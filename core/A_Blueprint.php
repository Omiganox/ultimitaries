<?php namespace Ultimitaries\Core;
/**
 * @version 0.4
 * @category  Plans de construction
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2021, Ultimitaries
 */

use Ultimitaries\Core\Modules\T_Abstraction;

/** Base de l'ensemble de classes "Blueprint". Elle fournit le comportement basique des méthodes en charge de la vérification des propriétés de chacune de ses classes filles. */
abstract class A_Blueprint {
  use T_Abstraction;

  /** @var  bool  $blueprintStatus Indique si l'objet est considéré comme valide depuis la dernière modification de ses propriétés obligatoires. */
  private bool $blueprintStatus = false;

  /** Valide le statut de l'objet quand la méthode "neededAreOk(0)" confirme les valeurs de toutes ses propriétés. */
  protected function confirmStatus(): bool {
    return $this->blueprintStatus = true;
  }

  /**
   * Avec la méthode "confirmStatus(0)", la surcharge de cette méthode est efficace en suivant ces trois règles :
   *  - Si la classe est abstraite, on se contente d'ajouter les règles de confirmation sur ses propriétés (sauf dans le cas où ses classes filles ne surchargent pas la méthode "allIsCorrect(0)") ;
   *  - Si la classe n'est pas abstraite mais n'a pas de classes filles, c'est elle qui doit employer les méthodes de confirmation ;
   *  - Si la classe n'est ni abstraite mais a des classes filles, elle devra d'abord récupérer le résultat de son parent, puis effectuer un appel à la méthode "hasNewNeeded(0)" avant de procéder à ses propres vérifications. Si cette règle n'est pas respectée à la lettre, le statut de la classe fille pourra être validé même si ses propres propriétés ne sont pas conformes aux règles de confirmation qui lui ont été attribuées.
   */
  protected function isConfirmed(): bool {
    return $this->blueprintStatus;
  }

  /** 
   * Vérifie si tous les propriétés obligatoires de l'instance appelante sont correctement renseignés. Des valeurs externes peuvent être utilisées pour renforcer la conformité de l'objet selon les opérations externes.
   * 
   * @param array $criterions [Optionnel] Les critères supplémentaires pour la vérification.
   */
  abstract public function isOK(array $criterions=[]): bool;

  /** Réinitialise le statut de l'objet quand une propriété obligatoire est modifiée par le biais de son setter. */
  protected function resetStatus(): self {
    $this->blueprintStatus = false;
    return $this;
  }
}
