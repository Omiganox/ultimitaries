<?php namespace Ultimitaries\Core\Security;
/**
 * @version 0.1
 * @category  Dé-crypteurs
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2022, Ultimitaries
 */

/** Les méthode de cette interface convertissent les tableaux et les objets "Countable" en un autre type de données, et inversement. */
interface I_ArrayCipher {
  /**
   * Décrypte une donnée quelconque en un tableau.
   * 
   * @param mixed $encoded  La donnée à décrypter.
   */
  function decodeArrayFrom(mixed $encoded): array|\Countable;

  /**
   * Crypte un tableau en une donnée d'un autre type.
   * 
   * @param array|\Countable  $array  Le tableau à crypter.
   */
  function encodeArrayTo(array|\Countable $array);
}