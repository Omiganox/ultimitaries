<?php namespace Ultimitaries\Core\Security;
/**
 * @version 0.1
 * @category  Dé-crypteurs
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2022, Ultimitaries
 */

/** Les méthode de cette interface convertissent les chaînes de caractères en un autre type de données, et inversement. */
interface I_StringCipher {
  /**
   * Décrypte une donnée quelconque en une chaîne de caractères.
   * 
   * @param mixed $encoded  La donnée à décrypter.
   */
  function decodeStringFrom(mixed $encoded): string;

  /**
   * Crypte une chaîne de caractères en une donnée d'un autre type.
   * 
   * @param string  $string La chaîne de caractères à crypter.
   */
  function encodeStringTo(string $string);
}