<?php namespace Ultimitaries\Core\Security;
/**
 * @version 0.1
 * @category  Dé-crypteurs
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2022, Ultimitaries
 */

/** Les méthode de cette interface intègrent les méthodes de dé-cryptage de l'utilisateur pour qu'elles puissent être utilisées dans le composant "EncryptedStorage" du framework. */
interface I_Cipher {
  /**
   * Décrypte une donnée codée.
   * 
   * @param mixed $encoded  La donnée à décrypter.
   */
  function decode(mixed $encoded): mixed;

  /**
   * Crypte une donnée.
   * 
   * @param mixed $values La donnée à crypter.
   */
  function encode(mixed $values): mixed;
}