<?php namespace Ultimitaries\Core\Security;
/**
 * @version 0.1
 * @category  Dé-crypteurs
 * @author  Rémi "Omiganox" KAJAK
 * @copyright 2022, Ultimitaries
 */

/** Les méthode de cette interface convertissent les nombres en un autre type de données, et inversement. */
interface I_NumericCipher {
  /**
   * Décrypte une donnée quelconque en un nombre entier ou décimal.
   * 
   * @param mixed $encoded  La donnée à décrypter.
   */
  function decodeStringFrom(mixed $encoded): int|float;

  /**
   * Crypte un nombre quelconque en une donnée d'un autre type.
   * 
   * @param int|float $number Le nombre à crypter.
   */
  function encodeStringTo(string $number);
}