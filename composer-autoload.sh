#!/bin/bash
if [ ! -z $1 ]; then
  dirTargeted="/var/www/html/${1}"

  if [ -e $dirTargeted ]; then
    cd $dirTargeted
    
    if [ -e "composer.json" ]; then
      composer dump-autoload -o
    else
      echo "File \"composer.json\" doesn't exist in directory \"${dirTargeted}\" !"
    fi
  else
    echo "Directory \"${dirTargeted}\" doesn't exist !"
  fi
else
  echo "Script needs an argument to work !"
fi