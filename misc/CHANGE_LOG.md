# 0.9.1
## Components
### LinkFinder (v5.3.0)
- Une nouvelle propriété privée a été ajouté pour décharger la méthode "search" de son troisième argument et ainsi éviter un gâchis de ressources lors de la récursion ;
- Plusieurs instructions ont été simplifiées ou combinées pour minifier davantage le code source.

__________________________________________________________________________________________________________________________________________________
# 0.9
## Général
- Amélioration de la convention de nommage et de l'organisation du framework :
    - Tous les noms des répertoires absents du fichier "composer.json" doivent être mis au pluriel (valables pour les espaces de noms éponymes : PSR-4) ;
    - Les fabriques abstraites perdent leur préfixe "F_" et deviennent des classes finales non-instanciables et non-clonables. Elles se trouvent désormais dans des répertoires nommés "Factories".
- Augmentation générale de la stabilité logiciel : le framework n'est pas encore tout à fait prêt pour une mise en prod, mais ça ne saurait tarder.

## Components
### LinkFinder (v5.2.4)
- Le constructeur gère les cas de corruption du fichier de sauvegarde des liens :
    - Désormais, le bloc try/catch capture aussi les erreurs fatales (le script n'est plus interrompu en cas de corruption détectée) ;
    - Il s'assure que la propriété "relativeLinks" est initialisée à la fin de la construction de l'objet.
- Simplifications de plusieurs instructions (suppression de variables inutiles, raccourcicement des noms de variable, etc) ;
- Le troisième paramètre de la méthode "search(5)" n'est plus optionnel.

## Core
- Suppression de la classe "A_CustomException" : toutes les exceptions personnalisées héritent directement de la classe native "Exception".

### A_Blueprint (v0.4)
- Renommage des méthodes "neededAreOk" et "hasNewNeeded" respectivement en "isOK" et "resetStatus" ;
- La méthode "isOK" possède désormais un argument optionnel de type tableau.

### A_Blueprint (v0.4) et A_Factory (v0.1.2)
- Le trait "T_Abstraction" remplace désormais le trait "T_Utils".

### Containers
#### Elem (v1.2)
- Suppression des méthodes "lockOff" et "lockOn" au profit du setter de la propriété "lock" (plus simple à utiliser, et pourquoi je continue de me compliquer la vie pour pas grand-chose ?).

#### T_Container (v0.1)
- Création du trait "T_Container" :
    - Implémentation des trait "T_Utils" et "T_Logs" ;
    - Importation de la méthode finale "isNotItself(1)" depuis le trait "T_ArrayContainer".

#### Arrays
- Création du dossier "ArrayObjects" : les classes "A_ArrayObject", "ArrayList" et "Map" y ont été déplacées (+ réadaptation des espaces de nom) ;
- Le dossier et l'espace de nom "FirstOut" ont été mis au pluriel pour rester cohérent avec la convention de nommage du framework.

##### T_ArrayContainer (v0.5)
- Les méthodes "__invoke(1+)" et "controlInsertion(2)" renvoient désormais la référence de l'objet ;
- Réécriture des méthodes "isFull(1)" et "isNotItself(1)" sous la forme de conditions ternaires ;
- L'implémentation des traits "T_Utils" et "T_Logs" est remplacée par celle du nouveau trait "T_Container".

##### A_ArrayObject (v0.4)
- Fix de la méthode "getIterator" : la propriété "iterator" est appelée à la place de l'ancienne classe par défaut ;
- Ajout de la méthode "isList".

##### ArrayList (v2.1)
- La méthode "isList" est finale et retourne toujours vrai.

### Modules
#### I_Usage (v0.2)
- Renommage de l'interface "I_StorageUsage" en "I_Usage" et déplacement dans le répertoire "Modules" : elle n'est plus exclusif aux classes "A_Storage".

#### T_Abstraction (v0.1)
- Création du trait "T_Abstraction" :
    - Importation de la méthode statique "matchConstant(2)" depuis le trait "T_Utils" ;
    - Ajout de la méthode "implementsMethods(1+)".

#### T_Utils (v0.5)
- Exportation de la méthode statique "matchConstant(2)" dans le trait "T_Utils".

#### Logs
- Renommage du dossier et de l'espace de nom "LogSystem" en "Logs".

##### T_Logs (v0.3)
- Renommage de "T_LogGenerator" en "T_Logs" ;
- La méthode "createLog(1)" retourne désormais faux.

### Parsers
#### Database
- Suppression des classes "Blueprints/DatabaseParser", "MySQL" et "F_DatabaseParser", et du trait "T_DatabaseSingleton".

##### DatabaseParser (v1.0)
- Réécriture de la classe "A_Database" en "DatabaseParser" :
    - Suppression des propriétés "connection" et "instance" au profit des nouvelles propriétés "activeSessions" et "sessionName" : la classe peut désormais générer plus d'un objet, mais seulement un par base de données ;
    - Le constructeur attend désormais une chaîne de caractères au lieu d'une instance "Blueprints/DatabaseParser" et ne gère plus la récupération des informations du fichier de configuration ;
    - Réécriture de la méthode "getInstance(1)" en "getInstanceFor(1)" :
        - La méthode gère la récupération des informations de la connexion aux base de données et vérifie qu'elles n'ont pas déjà été utilisées par une autre instance active ;
        - Elle lève une exception le cas échéant.
    - Récupération des méthodes implémentées dans la classe fille "MySQL" :
        - Désormais, la connexion à la base de données doit être effective pour les utiliser.
    - Suppression des méthodes "closeConnection", "createTable", "isInstanciated" et "openConnection" ;
    - Renommage de la méthode "executeSecuredQuery(1)" en "execute(1)" : les modifications sont les mêmes que celles de la méthode "controlKeywords(2)" de la classe "SQL".

#### TextFormat
##### A_TextFormat (v0.2)
- Renommage de la classe "A_TextFormatParser" en "A_TextFormat" ;
- La classe implémente directement le trait "T_SimpleSingleton" à la place de ses classes filles ;
- Implémentation du trait "T_Abstraction" : la méthode "decode" est maintenant surchargeable ;
- Réécriture de l'ancienne version de "decode(1)" en "sendLog(1)".

##### XML (v0.2)
- Suppression des visiteurs : après vérification du comportement des parsers natifs, leur implémentation n'est plus d'actualité ;
- La méthode "decode" comporte désormais deux arguments obligatoires ;
- La classe est devenu un singleton, mais est toujours un prototype : aucune utilisation prévue pour le moment.

### Storages
- Suppression de l'interface "I_BlueprintChecker" au profit d'une nouvelle implémentation plus souple ;
- Suppression de la classe "IncorrectBlueprintException" au profit de "InvalidArgumentException".

#### A_Storage (v0.5)
- Suppression des méthodes abstraites "add", "create", "delete", "get" et "update" :
    - Même si ce n'est pas idéal d'un point de vue conceptuel, les classes filles les implémentent toujours, mais avec des arguments adaptés ;
    - Implémentation du nouveau trait "T_Abstraction" et utilisation de la méthode "implementsMethods(1+)" dans le constructeur pour contrôler leur présence dans les classes filles.
- La méthode "canBeUsed" est désormais finale.

#### DatabaseStorage (v1.2)
- La classe n'implémente plus l'interface "I_BlueprintChecker" : la méthode "checkBlueprint(1)" a été supprimé ;
- Suppression des constantes de classes (devenues superflues) ;
- Ajout d'une nouvelle propriété de type "ArrayList" : "tables" contient tous les noms dont l'existence a été vérifié dans la base de données à laquelle l'objet est connecté. Son objectif est de réduire la charge d'exécution de la méthode "exist(2)" ;
- Ajout de la méthode "__debugInfo" ;
- Désormais, l'argument des méthodes "add", "create", "delete", "get" et "update" est un objet "SQL" ;
- Réécriture de la méthode "exist(2)" : désormais, on vérifie l'existence d'une table une seule fois dans la base de données. Si elle existe, on enregistre son nom dans la propriété "tables" pour les vérifications suivantes. Cependant, les opérations des objets "DatabaseParser" devront être encapsulées dans des blocs "try/catch" (pour les exceptions PDO) en cas de "suppressions indésirables" (hacks, mauvais pushs en prod, stagiaire, etc).

#### EncryptedStorage (v0.4)
- Désormais, l'argument des méthodes "add", "create", "delete", "get" et "update" est un objet "A_Blueprint" ;
- Ajout de la définition de la méthode "checkThis(2)".

#### TextFileStorage (v1.2)
- La classe n'implémente plus l'interface "I_BlueprintChecker" : la méthode "checkBlueprint(1)" a été réécrite en "checkThis(1)" ;
- Implémentation du trait "T_SimpleSingleton" : la classe est désormais un singleton ;
- Remplacement des try/catch au profit du collecteur de logs ;
- Désormais, l'argument des méthodes anciennement héritées est un objet :
    - "TextFileModification" pour les méthodes "add", "create" et "update" ;
    - "TextFileDeletion" pour la méthode "delete" ;
    - "TextFileGetter" pour la méthode "get".
- Ajout de la méthode "sendLog(1)" pour supprimer la répétitivité de l'envoi des logs dans les autres méthodes.

#### Blueprints
- La classe abstraite "A_StorageMethodBlueprint" a été supprimé : avec la nouvelle implémentation de la classe "A_Storage", le tri des objets "Blueprints" n'est plus nécessaire dans les classes filles (excepté pour les classes "Décorateurs").

##### A_TextFile (v0.4)
- La classe n'hérite plus de "A_StorageMethodBlueprint" mais directement de "A_Blueprint" ;
- Renommage en "A_TextFile" (le chemin indique déjà que la classe appartient à la catégorie des "Blueprints").

##### SQL (v1.2)
- La classe n'hérite plus de "A_StorageMethodBlueprint" mais directement de "A_Blueprint" ;
- Importation et réécriture de la méthode "extractTableName(1)" en "getTableName" depuis la classe "DatabaseStorage" ;
- Consolidation de la méthode "controlKeywords(2)" :
    - Renforcement de la vérification des mot-clés en fonction du type connu (notamment pour les "?") ;
    - Amélioration de la reconnaissance des listes de mot-clés en fonction du type reçu de l'argument "$keywords".
- La méthode "isOK" vérifie si la méthode utilisée dans un objet "DatabaseStorage" est la bonne.

#### Factories
##### Storage (v0.5)
- Modification de la méthode "getInstanceOf(1)" : les choix "TF_ES" et "TF_S" ne prennent plus le deuxième argument en compte pour la création du nouvel objet "TextFileStorage".

## Debug
### Tester (v1.0)
- Création de la classe :
    - Ajout du constructeur et de destructeur pour gérer la collecte des informations associées à la compilation des scripts test ;
    - Ajout de la méthode "print(3)".
__________________________________________________________________________________________________________________________________________________
# 0.8.3
## Général
- Suppression du préfixe "BP_" des plans de construction et renommage dans les classes concernées ;
- Raccourcicement du nom des classes impliquant des noms de formats/langages/outils.

## Components
### LinkFinder (v5.2.3)
- Nouvelle simplification du constructeur :
    - Suppression du premier try/catch ;
    - La propriété "appRoot" est redéfinie seulement si le premier argument est invalide (sinon, il est déjà attribué) ;
    - Le log généré en cas d'erreur n'est plus affiché dans la fenêtre (seulement dans les fichiers du répertoire "debug") ;
    - La propriété "storage" ne génère plus de log dans le cas où le fichier de sauvegarde n'existe pas.
- Renommage de la méthode "getLastDirName(1)" en "getLastDir(1)" ;
- Suppression des commentaires devenus inutiles ;
- Dans la méthode "search(5)", la définition de la variable "$result" a été supprimé au profit d'un opérateur de coalescence nul sur son retournement.

## Core
### Containers
##### Elem (v1.1.1)
- Ajout de la méthode "__toString".

#### Arrays
##### T_ArrayContainer (v0.4)
- Importation de la propriété "callback" et des méthodes "__invoke" et "push(1+)" depuis la classe "A_FirstOut" ;
- Modification de la méthode "controlInsertion(2)" :
    - l'argument de type callable est devenu optionnel : s'il est nul, on utilise la propriété "callback" par défaut ;
    - le tableau reçu n'est plus inversé : seule la méthode "__invoke" le fait (pour s'assurer que tous les éléments soient insérés de gauche à droite).
- Simplification de la condition dans la méthode "modifyLimit(1)".

##### A_ArrayObject (v0.3)
- Suppression de la méthode "push(1+)" : elle est désormais appelée depuis le trait "T_ArrayContainer" ;
- Ajout des méthodes "set(2)" à partir de la méthode "offsetSet(2)" (cette dernière a été réécrite) et "uksort(1)" ;
- Ajout de la propriété "iterator" et son setter : la méthode "getIterator(0)" a été modifié en conséquence.
- Réécriture de la méthode "sort(3)" :
    - Ajout d'un troisième argument optionnel ;
    - Les fonctions natives de tri sont désormais indirectement appelées avec la fonction "call_user_func_array" et elles peuvent être préfixées selon les besoins ;
    - Cependant, elle génère encore des warnings dans certains cas : conversions de tableaux en string ou des objets en float ; logique native, donc pas moyen de régler le souci directement (à moins de se plonger dans "php-src").
- Ajout de la méthode abstraite "unsetAll(2)" ;
- Suppression de la méthode "getKeysFor(2)" : les clés peuvent être récupérées par le biais de "__call" avec la fonction "array_keys".

##### ArrayList (v2.0.2)
- La méthode "unshift(1+)" insère désormais les éléments de gauche à droite ;
- Réécriture de la méthode "sort(3)" ;
- Implémentation de la méthode "unsetAll(2)" ;
- Petites réécritures de code et de documentation.

##### Map (v2.0.2)
- Fix d'un bug dans la méthode "toList(0)" : la configuration du nouvel objet encodait incorrectement les données de "storage" ;
- Réécriture de la méthode "ksort(2)" avec la nouvelle définition de la méthode héritée "sort(3)" ;
- Fix de la méthode "unsetAll(2)".

##### Classes "FirstOut"
###### T_FirstOut (v0.4)
- Le deuxième argument est désormais inversé avant d'être transmis à la méthode "controlInsertion(2)".
__________________________________________________________________________________________________________________________________________________
# 0.8.2
## Général
- Suppression des attributions inutiles dans les constructeurs des classes instanciables ;
- Retrait des accesseurs des définitions de méthode des interfaces (redondance) ;
- Amélioration des fichiers de tests.

## Components
### LinkFinder (v5.2.2)
- Suppression du second argument de la méthode "getLastDirName(1)" (il était devenu inutile) ;
- Simplification de plusieurs instructions (meilleur emploi des ternaires, suppression des répétitions au maximum, etc) ;
- Importation du module "T_StringMethods" pour améliorer la vérification des mot-clés dans la méthode "search(5)".

## Core
### A_Factory (v0.1.1)
- Suppression du type de retour de la méthode "getInstanceOf(2) : désormais, les types de retours peuvent être plus spécialisés dans les classes filles.

### Containers
#### I_LockSystem (v0.2)
- Renommage de l'interface en "I_LockSystem" ;
- Ajout d'un second argument optionnel dans la définition de la méthode "isLocked(2)" : la génération des logs liés devient optionnelle.

#### Arrays
##### T_ArrayContainer (v0.3)
- Implémentation du trait "T_Utils" et application dans la méthode "isNotItself(1)" ;
- Réécriture de la méthode "isFull(1)" :
    - La génération du log est maintenant interne à la méthode et s'effectue automatiquement si "storage" est plein ;
    - Ajout d'un paramètre optionnel : il sert à renseigner une valeur pour le message du log.
- Simplification de la condition dans la méthode "modifyLimit(1)".

##### A_ArrayObject (v0.2.1)
- Suppression de l'appel du trait "T_Utils" : désormais, il est automatiquement implémenté par le biais du trait "T_ArrayContainer" ;
- Fix de la méthode "offsetSet(2)" : désormais, l'insertion tient compte de la limite de "storage" tout en s'assurant que l'objet n'est pas lui-même inséré (plus fix de la levée d'un warning sur l'existence d'une clé absente).

### Modules
#### T_StringMethods (v0.5)
- Amélioration de la méthode "containsWord(2)" en "containsWords(3)" :
    - Le deuxième paramètre est désormais un tableau : la regex boucle jusqu'à ce qu'il soit complètement lu ;
    - Un troisième argument optionnel a été ajouté : passé par référence, il s'agit d'un tableau qui contiendra les termes manquants.

#### T_Utils (v0.3)
- Réécriture de la méthode "areSameValues(3)" en "areEquals(3)" :
    - Simplification de l'algorithme en utilisant l'opérateur "<=>".

### Storages
#### Classes "Blueprints"
##### BP_SqlQuery (v1.1.1)
- Simplification de deux instructions dans la méthode "controlKeywords(2)".

#### F_Storage (v0.4)
- Ajout de la constante publique "N_S" et réorganisation des autres constantes ;
- Modification de la méthode "getInstanceOf(1)" :
    - Le type de retour est désormais "I_StorageUsage" ;
    - La méthode retourne le singleton "NoStorage" par défaut (au lieu de null) ;
    - Suppression de la condition "matchConstant" qui contrôlait l'input initial.
__________________________________________________________________________________________________________________________________________________
# 0.8.1
## Général
- Application complète de la norme PSR-4 :
    - Simplification du fichier "composer.json" en ne gardant que les appels aux répertoires "core" et "components" ;
    - Suppression de certains sous-espaces de nom et réécriture des appels dans les classes concernées ;
    - Renommage des sous-répertoires de "core" en adéquation avec les espaces de noms du framework.
- Légère amélioration des tests (ajout du temps d'exécution à la fin des scripts).

## Components
### LinkFinder (v5.2.1)
- Modification de la méthode "getLinkFor(1+)" :
    - Modification de la convention de nommage des clés de la propiété "relativeLinks" ;
    - Simplification du log d'erreur généré dans le cas où un fichier est introuvable : seuls les mot-clés secondaires sont affichés, désormais (quand ils sont renseignés) ; auparavant, le répertoire d'exécution et le nom du fichier cible étaient aussi indiqués.
__________________________________________________________________________________________________________________________________________________
# 0.8
## Général
- Comme pour les exceptions, toutes les classes "Blueprints" ont été déplacées dans les répertoires appropriés plutôt que d'être réunies dans un seul et même répertoire ; de même, les espaces de nom ont été adaptés. Voici la nouvelle répartition dans l'arborescence :
    - "A_Blueprint" -> "core/" ;
    - "BP_DatabaseParser" -> "core/parsers/database/blueprints/" ;
    - et le reste dans "core/storages/blueprints/".
- Déplacement du répertoire "debug/" à la racine du framework (placé auparavant dans le répertoire "core/") ;
- Suppression de l'interface "I_Exception" : son implémentation s'avérait inutile, en fin de compte ;
- Refonte du système des fabriques abstraites :
    - Suppression des composants "A_FactoryBlueprint" et "I_AbstractFactory" ;
    - Création de la classe abstraite "A_Factory" à partir des composants supprimées et réécriture des classes "F_" (plus de détails dans leur bloc respectif) ;
    - Les plans de constructions des ensemble de classe ne contiendront plus que les données essentielles à la créations des nouveaux objets. Leurs constantes et leur propriété optionnelle "A_Blueprint" sont retirées au profit de la nouvelle structure "A_Factory".
- Traduction en anglais de toutes les exceptions et les messages retournés par les différents composants.

## Components
### LinkFinder (v5.2)
- Ajout d'un second paramètre optionnel dans le constructeur pour pouvoir désactiver le stockage des liens relatifs ;
- Fix d'un bug dans la méthode "createRelativeLink(1)" : le lien des fichiers placés au-dessus du répertoire instanciateur contenait un "/" en trop ;
- La méthode "search(5)" génère désormais un log si un répertoire ne peut pas être lu ; ce dernier ne sera pas fouillé.

## Core
### A_Factory (v0.1)
- Importation et réécriture de la définition de la méthode "createInstance" :
    - La méthode est toujours abstraite, mais a été renommée "getInstaceOf" ;
    - Elle accepte désormais deux arguments : un type entier ou une chaîne de caractères, et un plan de construction optionnel (aka les propriétés principales de l'ancienne classe "A_FactoryBlueprint").

### Containers
- Déplacement de la classe finale "Elem" à la base du répertoire "containers/" depuis le répertoire "containers/arrays/" : elle pourra être implémentée dans les futures classes "Container" ("TreeContainers" en tête de liste) ;
- Création de l'interface "I_DataLocker" qui contient les prototypes des méthodes "changeLock(1+)" et "isLocked(1)".

#### Arrays
##### T_ArrayContainer (v0.2)
- Ajout d'un limiteur du nombre d'éléments stockés :
    - Il est représenté par la propriété privée "limit" et est null (aka illimité) par défaut ;
    - Ajout des méthodes publiques "isFull(0)", "isLimited(0)" et "modifyLimit(1)".
- Ajout de la méthode "controlInsertion(3)" pour supprimer un duplicata du code source dans les classes "A_FirstOut", "A_ArrayObject" et "ArrayList" (+ application dans leurs méthodes d'insertion) ;
- Le trait utilise désormais le trait "T_LogGenerator", ce qui permet d'alléger les appels dans "A_ArrayObject" et "A_FirstOut".

##### A_ArrayObject (v0.2)
- Implémentation de la nouvelle interface "I_DataLocker" dans la définition de la classe ;
- Suppression de la méthode magique "__unset(1)" : son paramètre étant limité à une chaîne de caractères, elle était inutilisable pour la classe "ArrayList" (de plus, elle vise essentiellement les propriétés, ce qui est à éviter dans l'implémentation prévue de la classe "A_ArrayObject") ;
- Exportation de la méthode "unsertAll(1)" dans la classe "Map" : un comportement imprévu empêche son utilisation normale dans la classe "ArrayList" ;
- Ajout d'un argument optionnel dans la méthode "get" : s'il est vrai, l'objet "Elem" (aka la valeur et son verrou) associé à l'index renseigné sera retourné.

##### ArrayList (v2.0.1)
- Fix de la méthode "unset(1)" : la suppression de la donnée et la réindexation s'effectuent enfin correctement.

##### Map (v2.0.1)
- Fix de la méthode "toList(0)" : l'objet "ArrayList" retourné contient bien des objets de la classe "Elem".

##### A_FirstOut (v0.3)
- Ajout de la propriété "callback" : désormais, le nom de la fonction de rappel est défini lors de la construction de l'objet plutôt que par l'implémentation de la méthode "push(1+)" dans les classes filles :
    - Le constructeur a été modifié en conséquence : le premier argument est obligatoire et est le nom du callback ;
    - La méthode "push(1+)" n'est plus abstraite mais finale.
- Suppression de la méthode "avoidNestedArray(1)".

##### Queue (v1.1) et Stack (v1.1)
- Renommage de la classe "File" en "Queue" (traduction moins sujette à la confusion) ;
- Suppression de l'implémentation de la méthode "push" ;
- Mise en place d'un constructeur avec le token "..." en guise d'argument.

### Modules
#### T_SimpleSingleton (v0.1)
- Création du trait : les classes qui l'implémentent pourront se comporter comme des singletons basiques.

#### T_StringMethods (v0.4)
- Renommage de la méthode "removeUnwantedChars(2)" en "slugify(2)".

#### LogWriter (v1.1)
- Importation du contenu de la classe mère et suppression de celle-ci : son implémentation n'est plus nécessaire pour la future classe "LogReader" (classe full JS) ;
- Suppression du constructeur, du cloneur et de la méthode "getInstance(0)" au profit du nouveau trait "T_SimpleSingleton" ;
- Réécriture de la méthode "createNewLog(2)" en "add(2)" :
    - Le premier argument est désormais une chaîne de caractères (le nom de la classe d'où vient le log) ;
    - Le seconde argument est désormais une union typée et accepte les objets "Error" ;
    - Retrait de la génération de la date et de l'heure du nouveau log.
- Réécriture de la méthode "getLogFilesDatas(0)" en "selectFileToWrite(1)" ;
    - Elle comporte désormais un argument booléen (passage par référence) ;
    - Importation et simplification de l'algorithme de sélection depuis la méthode "__destruct".
- Le destructeur génère désormais la date et l'heure pour la publication des nouveaux logs :
    - Il comporte également l'instruction nécessaire à l'encodage JSON ; néanmois, elle restera désactivée tant que la classe "LogReader.js" n'aura pas été créée et achevée (la lecture des logs devient compliquée en JSON...).

#### T_LogGenerator (v0.2)
- Privatisation de la propriété "logWriter" : son accès est limité aux classes qui implémentent le trait (leurs classes filles n'y ont plus accès) ;
- Ajout de la méthode "createLog(1)" pour garantir la génération des logs dans n'importe quelle classe.

#### T_Utils (v0.2)
- Suppression de la méthode "isEmptyValue(1)" : la fonction native "empty" traite déjà tous les cas possibles ;
- Fix de la méthode "areSameValues(3)" : au lieu de retourner faux dans le dernier cas, on compare les éléments avec les opérateurs "==" ou "===" ;
- Ajout temporaire de la méthode "array_is_list(1)" (suppression programmée lors du passage à PHP 8.1) ;
- Réécriture de la méthode "matchConstants(2)" :
    - La méthode est désormais statique et est renommée en "matchConstant(2)" ;
    - Ses arguments sont la nom de la classe hôte et la valeur de la constante à trouver ;
    - Plus aucune exception n'est levée ; un booléen est retourné à la place.

### Parsers
#### A_DatabaseParser (v0.3) et T_DatabaseSingleton (v0.1)
- Amélioration de l'implémentation singleton :
    - Typage fort et nullable de la propriété "instance" (désormais privée) ;
    - Importation et réécriture de la méthode statique "getInstance(0)" depuis la classe "MySqlParser" ;
    - Création des méthodes statiques "isInstantiated(0)" et "destroyInstance(0)" ;
    - Création et implémentation du trait "T_DatabaseSingleton" pour éviter une future duplication du constructeur et du cloneur dans les classes filles.
- Les constructeurs de classe acceptent désormais un argument : un objet de la classe "BP_DatabaseParser".

#### BP_DatabaseParser (v1.1)
- La classe n'hérite plus de la classe "A_FactoryBlueprint" mais de la classe "A_Blueprint" ;
- Exportation des constantes dans la classe "F_DatabaseParser" ;
- Ajout des propriétés "fileLink" et "section", correspondant respectivement :
    - au lien relatif ou absolu du fichier de configuration (il n'est plus un paramètre fixe dans "A_DatabaseParser") ;
    - et au nom de la section qui contient les paramètres (optionnel).
- Suppression des propriétés "instanceType" et "specs".
- Réécriture de la méthode "neededAreOk(0)".

#### F_DatabaseParser (v0.3)
- La classe n'implémente plus l'interface "I_AbstractFactory" mais hérite désormais de la classe "A_Factory" ;
- Elle comporte désormais la constante "MySQL", auparavant détenue par la classe "BP_DatabaseParser" ;
- Ajout d'un contrôle d'existence sur l'instance unique de l'ensemble de classe "A_DatabaseParser" : si elle existe déjà et qu'on cherche à créer un autre type d'objet, l'instance est réinitialisée avant de procéder à la création du nouvel objet.

#### MySqlParser (v1.1)
- Léger renommage de la classe "MySQLParser" ;
- Exportation de la méthode "getInstance(0)" dans la classe mère.

#### A_TextFormatParser (v0.1), IniParser(v1.0), JsonParser (v1.0), XmlParser (v0.1) et F_TextFormatParser (v0.1)
- Création du nouveau module "Parser" dédié à l'interprétation des fichiers texte.

#### I_XmlVisitor (v0.1)
- Création de l'interface et de ses prototypes de méthode "doDecode(1)" et "doEncode(1)" :
    - Elle propose une implémentation personnalisée du design pattern "Visiteur" : si elle permet de définir efficacement des comportements identiques en fonction d'objets spécifiques, l'argument de ses méthodes n'attend pas un objet en guise de paramètre.

#### DefaultXmlVisitor (v0.1)
- Création de la classe :
    - Elle implémente l'interface "I_XmlVisitor" et utilise exclusivement les classes natives "XMLReader" et "XMLWriter".
    - Son implémentation n'est cependant pas terminée (raison principale : flemme).

### Storages
- Suppression de la classe "BP_Storage" : avec la refonte des fabriques abstraites, elle est devenue inutile.

#### I_BlueprintChecker (v0.1)
- Création de l'interface : importation de la définition de la méthode "checkBlueprint(1)" depuis la classe "A_Storage".

#### F_DatabaseParser (v0.3)
- La classe n'implémente plus l'interface "I_AbstractFactory" mais hérite désormais de la classe "A_Factory" ;
- Elle comporte désormais les constantes qui étaient auparavant détenues par la classe "BP_Storage".

#### A_StorageMethodBlueprint (v0.3)
- Renommage de la propriété "checkStatus" et de son getter respectivement en "rightType" et "isRightType" pour minimiser la confusion avec la propriété héritée "blueprintStatus".

#### A_Storage (v0.4)
- Suppression de la définition de la méthode "checkBlueprint(1)".

#### DatabaseStorage (v1.1) et TextFileStorage (v1.1)
- Implémentation de l'interface "I_BlueprintChecker" : la méthode "checkBlueprint(1)" devient publique ;
- Suppression des instructions liées aux plans de construction secondaires dans les constructeurs : désormais, la définition de l'objet n'enchaîne plus directement sur une opération de la classe (plus simple à gérer pour la maintenance du code).

#### EncryptedStorage (v0.3)
- Suppression de la méthode "checkBlueprint(1)".

#### TextFileStorage (v1.1)
- Suppression complète du constructeur (devenu inutile) ;
- Remplacement :
    - de quelques fonctions natives par d'autres dans la méthode "create(1)" pour rendre le code plus clair ;
    - des "switch" les moins complexes par des "match" ;
    - des appels de la méthode "getCheckStatus" par "isRightType".

#### BP_SqlQuery (v1.1)
- Réécriture de la classe :
    - Ajout de la méthode "getKeywordsNumber" ;
    - La méthode "controlKeywords(2)" gère désormais le traitement du mot-clé "?" et retourne le premier paramètre en cas de succès. Le cas échéant, une exception est levée en fonction de l'erreur ;
    - Modification en conséquence du constructeur et du getter de la propriété "keywords" ;
    - La méthode "neededAreOk(0)" ne gère plus la vérification des mot-clés et se contente de regarder si les propriétés sont vides ou pas en fonction du nombre de mot-clés détectés dans la requête SQL.

#### NoStorage (v1.1)
- Suppression de la propriété statique et des méthodes liées au singleton au profit du nouveau trait "T_SimpleSingleton".
__________________________________________________________________________________________________________________________________________________
# 0.7
## Général
- Migration du projet sur VS Code :
    - Ajout de documentation ou d'unions typées dans les classes pour supprimer les fausses erreurs d'implémentation détectées par l'extension Intelephense.
- Importation de tous les logs de version de chaque composant dans ce fichier : l'objectif est d'alléger au maximum les fichiers source ;
- Renommage du répertoire "dev-tools" en "core" ;
- Déplacement de la classe "LinkFinder" dans un nouveau répertoire nommé "components" et placé à la racine du framework ;
- Suppression du répertoire "exceptions" à la base du répertoire "core" : désormais, chaque ensemble de classes possède son propre répertoire d'exceptions. Les fichiers d'implémentation de ces dernières restent à la base de "core" ;
- Création de nouveaux sous-espaces de noms pour catégoriser davantage les composants ;
- Mise en place d'appels de composants dynamiques par le biais de Composer : l'objectif est de supprimer au maximum les appels "require/inclure" dans les fichiers source.

## Components
### LinkFinder (v5.1.1)
- Modification de la méthode "createRelativeLink(1)" : les liens de répertoires se terminent maintenant par un slash ;
- Intégration des modifications apportées à la classe "Map".

## Noyau
### Arrays
- Suppression de l'interface "I_ArrayContainer" au profit du nouveau trait "T_ArrayContainer".

#### T_ArrayContainer (v0.1)
- Création du trait : l'objectif est de centraliser les propriétés et méthodes communes aux classes abstraites "A_FirstOut" et "A_ArrayObject" ;
- Importation de la propriété "storage" et des méthodes "clear(0)", "count(0)" et "isNotItself(1)".

#### A_ArrayObject (v0.1)
- Création de la classe abstraite : son objectif est de remplacer "\ArrayObject" en intégrant directement le système de verrouillage de la classe "Map" (v1.1).

#### Map (v2.0)
- Réécriture complète de la classe : la majeure partie de ses méthodes ont été exportées dans "A_ArrayObject" et elle en hérite désormais.

#### ArrayList (v2.0)
- Réécriture complète de la classe : elle hérite désormais de la classe abstraite "A_ArrayObject" au lieu de "Map".

#### A_FirstOut (v0.2.2)

- Refactorisation du code avec l'intégration du nouveau trait "T_ArrayContainer" ;
- Ajout des méthodes "__invoke(1+)" et "avoidNestedArray(1)" ;
- La méthode "push(1+)" peut désormais accepter plus d'un argument.

#### LockedDataException (v0.2)
- Renommage de la classe en "LockedDataException".

### Modules
#### LogWriter (v1.0.2)
- Ajout de la méthode magique "__debugInfo(0)" : var_dump renvoie désormais le nombre de logs enregistrés par l'instance.

### Parsers
#### A_DatabaseParser (v0.2.2)
- Intégration des modifications apportées à la classe "Map".

### Storages
#### A_Storage (v0.3.1)
- Réinsertion du second argument de la méthode "exist(2)" et réimplémentation dans les classes filles.
__________________________________________________________________________________________________________________________________________________
# 0.1 -> 0.6
Voir les six premiers commits du dépôt distant pour les détails de chaque version
