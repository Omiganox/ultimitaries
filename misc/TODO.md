# PRIORITAIRE
## 0.10
### components/AccountManager
- Réécrire l'ancienne classe "AccountManager" avec les nouveaux composants du noyau.

### Parsers/TextFormats
- Finir l'implémentation de la classe "XML" :
  - S'inspirer du tutoriel trouvé en ligne pour écrire une méthode "decode" efficace (la plus rapide possible avec un coût mémoire moindre).

### Database[Storage,DatabaseParser]
- Inclure la gestion des transactions.

### TextFileStorage
- Intégrer la gestion des streams aux méthodes "get" et "update" (moins coûteux en mémoire et évite les plantages du serveur pour les fichiers lourds).

### EncryptedStorage
- Finir l'écriture de la classe : Terminer la méthode "add", puis adapter son script aux autres méthodes.

# SECONDAIRE
## Documentation technique
- Tout traduire en anglais un de ces jours.

## Dossier de conception
- Finir la remise au propre du diagramme de classe UML sur PC.

## Implémentations futures (version indéterminée)
### Trees
- Implémenter un ensemble de classes pour les arbres binaires et leur exploitation :
    - Créer une classe "SearchTree" quand les classes "A_Tree" et "BinaryTree" seront bien avancées.

### Classe "LogReader.js"
- Objectif : lire tous les fichiers logs du dossier "debug" et retourner tout leur contenu dans un tableau lisible, en classant les informations par colonne et par date (résultat final à déterminer) ;
- Remplacer le format actuel des logs par l'encodage JSON dans la classe "LogWriter" (l'instruction est déjà écrite) ;
- Définir la classe "LogReader" : réviser le JS en premier lieu.

### T_ArrayContainer
- Modifier la méthode "controlInsertion(3)" pour lui ajouter un nouvel argument optionnel :
    - "sync=true" : lorsqu'il est vrai, la boucle du script s'arrête quand le tableau est plein ; sinon, on considère que le tableau est exploité de manière asynchrone par - au moins - un processus parallèle.

### DatabaseParser
- Si possible, améliorer le temps de traitement des opérations :
    - Sachant qu'une opération effectué par un objet peut comptabiliser plus d'une requête SQL exécutée et que la VM exécute d'autres tâches en fond (certains moments, le serveur test est plus lent que d'habitude) :
        - Dans le fichier "testDatabaseParser", pour 7 opérations : min ~= 0.09 sec / max ~= 1.2 sec ;
        - Dans le fichier "testStorage", pour 9 opérations avec la classe "DatabaseStorage" : min ~= 0.32191 sec / max ~= 2.38475 (exceptionnellement : 3.29 à 4.5 secs) ;
    - Pour le second fichier, la majeure partie des tests s'exécutent dans une fourchette de 0.3 à 1.5 secs (les 2 secs sont rarement dépassés).